import 'package:iec_covid_19_flutter/models/region_model.dart';

abstract class RegionRepositoryInterface {
  Future<List<RegionModel>> all();
}
