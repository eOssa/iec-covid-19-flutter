import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/models/gender_model.dart';
import 'package:iec_covid_19_flutter/repositories/gender_repository_interface.dart';
import 'package:meedu/meedu.dart';

class GenderRepository implements GenderRepositoryInterface {
  @override
  Future<List<GenderModel>> all() async {
    List<GenderModel> genders = [];

    List<Map<String, dynamic>?> data = await Get.i
        .find<DatabaseConnector>()
        .all(Get.i.find<GenderModel>().table);

    data.forEach((value) {
      GenderModel type = GenderModel.fromJson(value!);
      return genders.add(type);
    });
    genders.sort((a, b) => a.name.compareTo(b.name));
    return genders;
  }
}
