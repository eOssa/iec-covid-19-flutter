import 'package:iec_covid_19_flutter/models/comorbidity_model.dart';

abstract class ComorbidityRepositoryInterface {
  Future<List<ComorbidityModel>> all();
  Future<List<String>> getNames(List<int> ids);
  Future<ComorbidityModel> store(String name);
}
