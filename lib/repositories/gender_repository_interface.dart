import 'package:iec_covid_19_flutter/models/gender_model.dart';

abstract class GenderRepositoryInterface {
  Future<List<GenderModel>> all();
}
