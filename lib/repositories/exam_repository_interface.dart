import 'package:iec_covid_19_flutter/models/exam_model.dart';

abstract class ExamRepositoryInterface {
  Future<List<ExamModel>> all();
  Future<String> findName(int id);
}
