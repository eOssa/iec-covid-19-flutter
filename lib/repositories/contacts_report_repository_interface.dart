import 'package:iec_covid_19_flutter/models/iec_model.dart';

abstract class ContactsReportRepositoryInterface {
  Future<Map<String, List<int>>> generate(IecModel iec);
}
