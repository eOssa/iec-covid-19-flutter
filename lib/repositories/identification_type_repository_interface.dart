import 'package:iec_covid_19_flutter/models/identification_type_model.dart';

abstract class IdentificationTypeRepositoryInterface {
  Future<List<IdentificationTypeModel>> all();
}
