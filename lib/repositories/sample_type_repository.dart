import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/models/sample_type_model.dart';
import 'package:iec_covid_19_flutter/repositories/sample_type_repository_interface.dart';
import 'package:meedu/meedu.dart';

class SampleTypeRepository implements SampleTypeRepositoryInterface {
  @override
  Future<List<SampleTypeModel>> all() async {
    List<SampleTypeModel> samplesTypes = [];

    List<Map<String, dynamic>?> data = await Get.i
        .find<DatabaseConnector>()
        .all(Get.i.find<SampleTypeModel>().table);

    data.forEach((value) {
      SampleTypeModel type = SampleTypeModel.fromJson(value!);
      return samplesTypes.add(type);
    });
    samplesTypes.sort((a, b) => a.name.compareTo(b.name));
    return samplesTypes;
  }
}
