import 'package:iec_covid_19_flutter/models/sample_type_model.dart';

abstract class SampleTypeRepositoryInterface {
  Future<List<SampleTypeModel>> all();
}
