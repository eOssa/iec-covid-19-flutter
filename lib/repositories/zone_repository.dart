import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/models/zone_model.dart';
import 'package:iec_covid_19_flutter/repositories/zone_repository_interface.dart';
import 'package:meedu/meedu.dart';

class ZoneRepository implements ZoneRepositoryInterface {
  @override
  Future<List<ZoneModel>> all() async {
    List<ZoneModel> zones = [];

    List<Map<String, dynamic>?> data = await Get.i
        .find<DatabaseConnector>()
        .all(Get.i.find<ZoneModel>().table);

    data.forEach((value) => zones.add(ZoneModel.fromJson(value!)));
    zones.sort((a, b) => a.name.compareTo(b.name));
    return zones;
  }
}
