import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/models/identification_type_model.dart';
import 'package:iec_covid_19_flutter/repositories/identification_type_repository_interface.dart';
import 'package:meedu/meedu.dart';

class IdentificationTypeRepository
    implements IdentificationTypeRepositoryInterface {
  @override
  Future<List<IdentificationTypeModel>> all() async {
    List<IdentificationTypeModel> identificationsTypes = [];

    List<Map<String, dynamic>> data = await Get.i
        .find<DatabaseConnector>()
        .all(Get.i.find<IdentificationTypeModel>().table);

    data.forEach((value) {
      IdentificationTypeModel type = IdentificationTypeModel.fromJson(value);
      return identificationsTypes.add(type);
    });
    identificationsTypes.sort((a, b) => a.name.compareTo(b.name));
    return identificationsTypes;
  }
}
