import 'dart:convert' show utf8;

import 'package:iec_covid_19_flutter/api/routes.dart';
import 'package:iec_covid_19_flutter/http/client_interface.dart';
import 'package:iec_covid_19_flutter/models/iec_model.dart';
import 'package:iec_covid_19_flutter/repositories/iecs_report_repository_interface.dart';
import 'package:meedu/meedu.dart';

class IecsReportApiRepository implements IecsReportRepositoryInterface {
  Future<Map<String, List<int>>> generate(List<IecModel> iecs) async {
    List<Map<String, dynamic>> data = await Future.wait<Map<String, dynamic>>(
      iecs.map((iec) async {
        Map<String, dynamic> iecMap = await iec.toIecReportMap();
        iecMap['iecContacts'] = await iec.toContactsReportMap();
        return iecMap;
      }),
    );
    final res = await Get.i.find<HttpClientInterface>().post(
      Routes.iecsReport,
      data: {"iecs": data},
      options: {'response_type': 'bytes'},
    );
    String header = res.headers.map['content-disposition'][0];
    String filename = header.substring(22, header.length - 1);
    filename = utf8.decode(filename.runes.toList());
    return {filename: res.data};
  }
}
