import 'package:iec_covid_19_flutter/models/zone_model.dart';

abstract class ZoneRepositoryInterface {
  Future<List<ZoneModel>> all();
}
