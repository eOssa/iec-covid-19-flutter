import 'package:iec_covid_19_flutter/models/medication_model.dart';

abstract class MedicationRepositoryInterface {
  Future<List<MedicationModel>> all();
  Future<List<String>> getNames(List<int> ids);
  Future<MedicationModel> store(String name);
}
