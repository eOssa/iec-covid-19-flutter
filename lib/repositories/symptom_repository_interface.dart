import 'package:iec_covid_19_flutter/models/symptom_model.dart';

abstract class SymptomRepositoryInterface {
  Future<List<SymptomModel>> all();
  Future<List<String>> getNames(List<int> ids);
  Future<SymptomModel> store(String name);
}
