import 'package:iec_covid_19_flutter/models/city_model.dart';

abstract class CityRepositoryInterface {
  Future<List<CityModel>> getByRegion(regionId);
}
