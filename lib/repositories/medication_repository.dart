import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/models/medication_model.dart';
import 'package:iec_covid_19_flutter/repositories/medication_repository_interface.dart';
import 'package:meedu/meedu.dart';

class MedicationRepository implements MedicationRepositoryInterface {
  @override
  Future<List<MedicationModel>> all() async {
    List<MedicationModel> medications = [];

    List<Map<String, dynamic>?> data = await Get.i
        .find<DatabaseConnector>()
        .all(Get.i.find<MedicationModel>().table);

    data.forEach((value) => medications.add(MedicationModel.fromJson(value!)));
    medications.sort((a, b) => a.name.compareTo(b.name));
    return medications;
  }

  Future<List<String>> getNames(List<int> ids) async {
    List<String> medications = [];

    List<Map<String, dynamic>?> data = await Get.i
        .find<DatabaseConnector>()
        .findMany(Get.i.find<MedicationModel>().table, ids);

    data.forEach((v) => medications.add(v!['name']));
    return medications;
  }

  @override
  Future<MedicationModel> store(String name) async {
    MedicationModel medication = MedicationModel(name: name);
    await medication.insert();
    return medication;
  }
}
