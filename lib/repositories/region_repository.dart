import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/models/region_model.dart';
import 'package:iec_covid_19_flutter/repositories/region_repository_interface.dart';
import 'package:meedu/meedu.dart';

class RegionRepository implements RegionRepositoryInterface {
  @override
  Future<List<RegionModel>> all() async {
    List<RegionModel> regions = [];

    List<Map<String, dynamic>?> data = await Get.i
        .find<DatabaseConnector>()
        .all(Get.i.find<RegionModel>().table);

    data.forEach((value) {
      RegionModel type = RegionModel.fromJson(value!);
      return regions.add(type);
    });
    regions.sort((a, b) => a.name.compareTo(b.name));
    return regions;
  }
}
