import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/models/comorbidity_model.dart';
import 'package:iec_covid_19_flutter/repositories/comorbidity_repository_interface.dart';
import 'package:meedu/meedu.dart';

class ComorbidityRepository implements ComorbidityRepositoryInterface {
  @override
  Future<List<ComorbidityModel>> all() async {
    List<ComorbidityModel> comorbidities = [];

    List<Map<String, dynamic>?> data = await Get.i
        .find<DatabaseConnector>()
        .all(Get.i.find<ComorbidityModel>().table);

    data.forEach((v) => comorbidities.add(ComorbidityModel.fromJson(v!)));
    comorbidities.sort((a, b) => a.name.compareTo(b.name));
    return comorbidities;
  }

  Future<List<String>> getNames(List<int> ids) async {
    List<String> comorbidities = [];

    List<Map<String, dynamic>?> data = await Get.i
        .find<DatabaseConnector>()
        .findMany(Get.i.find<ComorbidityModel>().table, ids);

    data.forEach((v) => comorbidities.add(v!['name']));
    return comorbidities;
  }

  @override
  Future<ComorbidityModel> store(String name) async {
    ComorbidityModel comorbidity = ComorbidityModel(name: name);
    await comorbidity.insert();
    return comorbidity;
  }
}
