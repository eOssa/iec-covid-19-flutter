import 'dart:convert' show utf8;

import 'package:iec_covid_19_flutter/api/routes.dart';
import 'package:iec_covid_19_flutter/http/client_interface.dart';
import 'package:iec_covid_19_flutter/models/iec_model.dart';
import 'package:iec_covid_19_flutter/repositories/iec_report_repository_interface.dart';
import 'package:meedu/meedu.dart';

class IecReportApiRepository implements IecReportRepositoryInterface {
  Future<Map<String, List<int>>> generate(IecModel iec) async {
    print(Routes.iecReport);
    final res = await Get.i.find<HttpClientInterface>().post(
      Routes.iecReport,
      data: await iec.toIecReportMap(),
      options: {'response_type': 'bytes'},
    );
    String header = res.headers.map['content-disposition'][0];
    String filename = header.substring(22, header.length - 1);
    filename = utf8.decode(filename.runes.toList());
    return {filename: res.data};
  }
}
