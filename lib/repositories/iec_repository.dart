import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/db/query_builder.dart';
import 'package:iec_covid_19_flutter/helpers/date.dart';
import 'package:iec_covid_19_flutter/models/iec_model.dart';
import 'package:iec_covid_19_flutter/repositories/iec_repository_interface.dart';
import 'package:meedu/meedu.dart';

class IecRepository implements IecRepositoryInterface {
  @override
  Future<bool> deleteIec(int id) async {
    return await Get.i
        .find<DatabaseConnector>()
        .delete(Get.i.find<IecModel>().table, id);
  }

  @override
  Future<List<IecModel>> getIecs({
    required int perPage,
    required int page,
    DateTime? endDate,
    String? identification,
    String? name,
    bool? onlyEffectives,
    DateTime? startDate,
  }) async {
    final QueryBuilder query = getBuilder(
      endDate: endDate,
      startDate: startDate,
      identification: identification,
      name: name,
      onlyEffectives: onlyEffectives,
    );
    List<IecModel> iecs = [];
    List<Map<String, dynamic>> data = await query.get();
    data.forEach((value) => iecs.add(IecModel.fromJson(value)));
    iecs.sort((a, b) => b.date!.compareTo(a.date!));
    return iecs;
  }

  @override
  Future<IecModel> showIec(int id) async {
    var iecData = await Get.i
        .find<DatabaseConnector>()
        .find(Get.i.find<IecModel>().table, id);
    return IecModel.fromJson(iecData!);
  }

  @override
  Future<int> store(IecModel iec) async {
    await iec.save();
    return iec.id;
  }

  @override
  Future<int> total({
    DateTime? endDate,
    String? identification,
    String? name,
    bool? onlyEffectives,
    DateTime? startDate,
  }) async {
    final QueryBuilder query = getBuilder(
      endDate: endDate,
      startDate: startDate,
      identification: identification,
      name: name,
      onlyEffectives: onlyEffectives,
    );
    return query.count();
  }

  QueryBuilder getBuilder({
    DateTime? endDate,
    String? identification,
    String? name,
    bool? onlyEffectives,
    DateTime? startDate,
  }) {
    final QueryBuilder query = Get.i.find<IecModel>().query();
    if (name != null && name.isNotEmpty) {
      query.where('contact.name', operator: 'like', value: '%$name%');
    }
    if (identification != null && identification.isNotEmpty) {
      query.where('contact.identification', value: identification);
    }
    if (onlyEffectives != null) {
      query.where('effective', value: onlyEffectives);
    }
    if (startDate != null) {
      query.where(
        'date',
        operator: '>=',
        value: DateHelper.format(startDate, format: 'yyyy-MM-dd HH:mm:ss'),
      );
    }
    if (endDate != null) {
      endDate = DateHelper.endOfDay(endDate);
      query.where(
        'date',
        operator: '<=',
        value: DateHelper.format(endDate, format: 'yyyy-MM-dd HH:mm:ss'),
      );
    }
    return query;
  }
}
