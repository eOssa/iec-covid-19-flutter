import 'package:iec_covid_19_flutter/models/iec_model.dart';

abstract class IecsReportRepositoryInterface {
  Future<Map<String, List<int>>> generate(List<IecModel> iec);
}
