import 'package:iec_covid_19_flutter/models/city_model.dart';
import 'package:iec_covid_19_flutter/repositories/city_repository_interface.dart';
import 'package:meedu/meedu.dart';

class CityRepository implements CityRepositoryInterface {
  @override
  Future<List<CityModel>> getByRegion(regionId) async {
    List<CityModel> cities = [];

    var query = Get.i.find<CityModel>().query()
      ..where('region_id', value: regionId);
    var data = await query.get();
    data.forEach((value) => cities.add(CityModel.fromJson(value)));
    cities.sort((a, b) => a.name.compareTo(b.name));
    return cities;
  }
}
