import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/models/exam_model.dart';
import 'package:iec_covid_19_flutter/repositories/exam_repository_interface.dart';
import 'package:meedu/meedu.dart';

class ExamRepository implements ExamRepositoryInterface {
  @override
  Future<List<ExamModel>> all() async {
    List<ExamModel> exams = [];

    List<Map<String, dynamic>?> data = await Get.i
        .find<DatabaseConnector>()
        .all(Get.i.find<ExamModel>().table);

    data.forEach((value) => exams.add(ExamModel.fromJson(value!)));
    exams.sort((a, b) => a.name.compareTo(b.name));
    return exams;
  }

  Future<String> findName(int id) async {
    Map<String, dynamic>? data = await Get.i
        .find<DatabaseConnector>()
        .find(Get.i.find<ExamModel>().table, id);
    if (data == null) {
      return "";
    }
    return data['name'];
  }
}
