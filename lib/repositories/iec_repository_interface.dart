import 'package:iec_covid_19_flutter/models/iec_model.dart';

abstract class IecRepositoryInterface {
  Future<bool> deleteIec(int id);

  Future<List<IecModel>> getIecs({
    required int perPage,
    required int page,
    DateTime? endDate,
    String? identification,
    String? name,
    bool? onlyEffectives,
    DateTime? startDate,
  });

  Future<IecModel> showIec(int id);

  Future<int> store(IecModel iec);

  Future<int> total({
    DateTime? endDate,
    String? identification,
    String? name,
    bool? onlyEffectives,
    DateTime? startDate,
  });
}
