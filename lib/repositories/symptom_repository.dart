import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/models/symptom_model.dart';
import 'package:iec_covid_19_flutter/repositories/symptom_repository_interface.dart';
import 'package:meedu/meedu.dart';

class SymptomRepository implements SymptomRepositoryInterface {
  @override
  Future<List<SymptomModel>> all() async {
    List<SymptomModel> symptoms = [];

    List<Map<String, dynamic>?> data = await Get.i
        .find<DatabaseConnector>()
        .all(Get.i.find<SymptomModel>().table);

    data.forEach((value) => symptoms.add(SymptomModel.fromJson(value!)));
    symptoms.sort((a, b) => a.name.compareTo(b.name));
    return symptoms;
  }

  Future<List<String>> getNames(List<int> ids) async {
    List<String> symptoms = [];

    List<Map<String, dynamic>?> data = await Get.i
        .find<DatabaseConnector>()
        .findMany(Get.i.find<SymptomModel>().table, ids);

    data.forEach((v) => symptoms.add(v!['name']));
    return symptoms;
  }

  @override
  Future<SymptomModel> store(String name) async {
    SymptomModel symptom = SymptomModel(name: name);
    await symptom.insert();
    return symptom;
  }
}
