import 'package:iec_covid_19_flutter/db/query_builder.dart';

abstract class DatabaseConnector {
  Future<List<Map<String, dynamic>>> all(String table);

  Future<int> count(String table, QueryBuilder? builder);

  Future<bool> delete(String table, int key);

  Future<Map<String, dynamic>?> find(String table, int key);

  Future<List<Map<String, dynamic>>> findMany(String table, List<int> keys);

  Future<List<Map<String, dynamic>>> get(
    String table,
    QueryBuilder? builder,
  );

  Future<int> insert(String table, Map<String, dynamic> values);

  Future<int> update(String table, int id, Map<String, dynamic> values);
}
