import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:meedu/meedu.dart';

class QueryBuilder {
  String table;
  List<Map<String, dynamic>> wheres = [];

  QueryBuilder(this.table);

  void where(String column, {String? operator, dynamic value}) {
    wheres.add({'column': column, 'operator': operator ?? '=', 'value': value});
  }

  Future<int> count() {
    return Get.i.find<DatabaseConnector>().count(table, this);
  }

  Future<List<Map<String, dynamic>>> get() {
    return Get.i.find<DatabaseConnector>().get(table, this);
  }
}
