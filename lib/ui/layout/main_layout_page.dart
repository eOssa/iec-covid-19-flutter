import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/widgets/navbar.dart';

class MainLayoutPage extends StatelessWidget {
  final Widget child;
  const MainLayoutPage({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            NavBar(),
            Expanded(child: child),
          ],
        ),
      ),
    );
  }
}
