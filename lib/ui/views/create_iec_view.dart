import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:flutter_meedu/router.dart' as router;
import 'package:iec_covid_19_flutter/widgets/header.dart';
import 'package:iec_covid_19_flutter/widgets/header_button.dart';

import 'package:iec_covid_19_flutter/controllers/contacts_report_controller.dart';
import 'package:iec_covid_19_flutter/controllers/create_iec_controller.dart';
import 'package:iec_covid_19_flutter/controllers/iec_report_controller.dart';
import 'package:iec_covid_19_flutter/widgets/app_container.dart';
import 'package:iec_covid_19_flutter/widgets/button_primary.dart';
import 'package:iec_covid_19_flutter/widgets/button_secondary.dart';
import 'package:iec_covid_19_flutter/widgets/button_warning.dart';
import 'package:iec_covid_19_flutter/widgets/form_iec/form_comorbidities.dart';
import 'package:iec_covid_19_flutter/widgets/form_iec/form_contact.dart';
import 'package:iec_covid_19_flutter/widgets/form_iec/form_contacts_related.dart';
import 'package:iec_covid_19_flutter/widgets/form_iec/form_history_medical.dart';
import 'package:iec_covid_19_flutter/widgets/form_iec/form_history_risk.dart';
import 'package:iec_covid_19_flutter/widgets/form_iec/form_laboratory.dart';
import 'package:iec_covid_19_flutter/widgets/form_iec/form_note_nursing.dart';
import 'package:iec_covid_19_flutter/widgets/form_panel/separator.dart';

final createIecProvider =
    SimpleProvider.withTag((ref) => CreateIecController(ref.arguments));
final contactsReportProvider =
    SimpleProvider((ref) => ContactsReportController());
final iecReportProvider = SimpleProvider((ref) => IecReportController());

class CreateIecView extends StatelessWidget {
  final int id;
  final ScrollController _scrollController = ScrollController();

  CreateIecView({Key? key, this.id = 0}) : super(key: key) {
    createIecProvider.find('iecs.$id').setArguments(id);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(createIecProvider.find('iecs.$id'));
        final crCtrl = ref.watch(contactsReportProvider);
        final irCtrl = ref.watch(iecReportProvider);
        return Column(
          children: [
            Header(
              text: 'Agregar IEC',
              actions: [
                if (ctrl.iec!.id != 0)
                  HeaderButton(
                    icon: Icon(Icons.grid_on),
                    child: Text('Generar Excel'),
                    color: Color(0xFF04733B),
                    disabled: ctrl.disabledContactsIecReport,
                    onPressed: () async {
                      ctrl.setDisabledContactsIecReport(true);
                      try {
                        await crCtrl.generate(ctrl.iec!);
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text('Excel descargado'),
                            backgroundColor: const Color(0xFF059669),
                          ),
                        );
                      } catch (e) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(
                              'Ocurrió un error, verifica tu conexión a internet',
                            ),
                            backgroundColor: const Color(0xFFE53935),
                          ),
                        );
                      }
                      ctrl.setDisabledContactsIecReport(false);
                    },
                  ),
                if (ctrl.iec!.id != 0)
                  HeaderButton(
                    icon: Icon(Icons.description),
                    child: Text('Generar Word'),
                    color: Color(0xFF2773DC),
                    disabled: ctrl.disabledIecsReport,
                    onPressed: () async {
                      ctrl.setDisabledIecsReport(true);
                      try {
                        await irCtrl.generate(ctrl.iec!);
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text('Word descargado'),
                            backgroundColor: const Color(0xFF059669),
                          ),
                        );
                      } catch (e) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(
                              'Ocurrió un error, verifica tu conexión a internet',
                            ),
                            backgroundColor: const Color(0xFFE53935),
                          ),
                        );
                      }
                      ctrl.setDisabledIecsReport(false);
                    },
                  ),
                if (ctrl.iec!.id != 0)
                  HeaderButton(
                    icon: Icon(Icons.add_circle_outlined),
                    child: Text('Nueva IEC'),
                    onPressed: () {
                      router.popAndPushNamed('/create-iec');
                    },
                  ),
              ],
            ),
            Expanded(
              child: SingleChildScrollView(
                controller: _scrollController,
                child: AppContainer(
                  child: Form(
                    key: ctrl.formKey,
                    child: Column(
                      key: ObjectKey(ctrl.iec),
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Stack(
                          children: <Widget>[
                            FormContact(id: id),
                            if (ctrl.existsDraft)
                              Positioned(
                                top: 0,
                                right: 0,
                                child: Row(
                                  children: <Widget>[
                                    ButtonSecondary(
                                      child: Text('Cargar borrador'),
                                      onPressed: ctrl.loadDraft,
                                    ),
                                    SizedBox(width: 8),
                                    ButtonWarning(
                                      child: Text('Borrar borrador'),
                                      onPressed: ctrl.deleteDraft,
                                    ),
                                  ],
                                ),
                              ),
                          ],
                        ),
                        FormPanelSeparator(),
                        FormHistoryRisk(id: id),
                        FormPanelSeparator(),
                        FormHistoryMedical(id: id),
                        FormPanelSeparator(),
                        FormComorbidities(id: id),
                        FormPanelSeparator(),
                        FormLaboratory(id: id),
                        FormPanelSeparator(),
                        FormNoteNursing(id: id),
                        FormPanelSeparator(),
                        FormContactsRelated(id: id),
                        Container(
                          alignment: Alignment.centerRight,
                          color: Colors.grey.shade50,
                          padding: EdgeInsets.symmetric(
                            horizontal: 24,
                            vertical: 12,
                          ),
                          child: ButtonPrimary(
                            child: Text('Guardar IEC'),
                            onPressed: () async {
                              if (ctrl.formKey.currentState!.validate()) {
                                await ctrl.storeIec();
                                router.popAndPushNamed('/iecs');
                              } else {
                                _scrollController.animateTo(
                                  _scrollController.position.minScrollExtent,
                                  duration: Duration(seconds: 2),
                                  curve: Curves.fastOutSlowIn,
                                );
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
