import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:flutter_meedu/router.dart' as router;
import 'package:iec_covid_19_flutter/controllers/iecs_report_controller.dart';
import 'package:iec_covid_19_flutter/widgets/app_select.dart';
import 'package:iec_covid_19_flutter/widgets/button_warning.dart';
import 'package:iec_covid_19_flutter/widgets/header.dart';
import 'package:iec_covid_19_flutter/widgets/header_button.dart';

import 'package:iec_covid_19_flutter/controllers/iecs_controller.dart';
import 'package:iec_covid_19_flutter/widgets/app_container.dart';
import 'package:iec_covid_19_flutter/widgets/app_input.dart';
import 'package:iec_covid_19_flutter/widgets/app_input_date.dart';
import 'package:iec_covid_19_flutter/widgets/app_table.dart';
import 'package:iec_covid_19_flutter/widgets/button_danger.dart';
import 'package:iec_covid_19_flutter/widgets/button_primary.dart';
import 'package:iec_covid_19_flutter/widgets/button_secondary.dart';
import 'package:iec_covid_19_flutter/widgets/form_group.dart';

final iecsProvider = SimpleProvider((ref) => IecsController());
final iecsReportProvider = SimpleProvider((ref) => IecsReportController());

class IecsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(iecsProvider);
        final irCtrl = ref.watch(iecsReportProvider);
        return Column(
          children: [
            Header(
              text: 'Listado de IECs',
              actions: [
                HeaderButton(
                  icon: Icon(Icons.folder_open),
                  color: Colors.blueAccent,
                  child: Text('Informe'),
                  onPressed: () => irCtrl.generate(ctrl.iecs),
                ),
                HeaderButton(
                  icon: Icon(Icons.add_circle_outlined),
                  child: Text('Nueva IEC'),
                  onPressed: () {
                    router.pushNamed('/create-iec');
                  },
                ),
              ],
            ),
            Expanded(
              child: SingleChildScrollView(
                child: AppContainer(
                  child: Column(
                    children: <Widget>[
                      AdvancedSearch(ctrl: ctrl),
                      Divider(),
                      SizedBox(
                        // width: double.infinity,
                        child: AppTable(
                          columns: [
                            'date',
                            'name',
                            'identification',
                            'effective',
                            'action',
                          ],
                          data: ctrl.iecsMaps,
                          options: {
                            'headings': {
                              'date': 'Fecha',
                              'effective': '¿Efectivo?',
                              'identification': 'ID',
                              'name': 'Nombre',
                              'action': '',
                            },
                          },
                          customColumns: {
                            'action': (item) {
                              return Row(
                                children: [
                                  ButtonSecondary(
                                    child: Text('Modificar'),
                                    onPressed: () {
                                      router.pushNamed(
                                        '/create-iec',
                                        arguments: item['id'],
                                      );
                                    },
                                  ),
                                  SizedBox(width: 16),
                                  ButtonDanger(
                                    child: Text('Eliminar'),
                                    onPressed: () {
                                      showDialog(
                                        context: context,
                                        builder: (_) => AlertDialog(
                                          title: Text('Eliminar IEC'),
                                          content: Text(
                                            '¿Está seguro de eliminar la IEC?',
                                          ),
                                          actions: [
                                            TextButton(
                                              child: Text('Sí'),
                                              onPressed: () {
                                                ctrl.deleteIec(item);
                                                router.pop();
                                              },
                                            ),
                                            TextButton(
                                              child: Text('No'),
                                              onPressed: () => router.pop(),
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                  ),
                                ],
                              );
                            },
                          },
                          perPage: ctrl.perPage,
                          totalRecords: ctrl.total,
                          onRowsPerPageChanged: (int? rows) {
                            if (rows != null) {
                              ctrl.updatePerPage(rows);
                            }
                          },
                          onPageChanged: (int page) {
                            ctrl.updatePage(page);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}

class AdvancedSearch extends StatelessWidget {
  const AdvancedSearch({
    Key? key,
    required this.ctrl,
  }) : super(key: key);

  final IecsController ctrl;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: double.infinity,
      height: size.width > 425 ? 210.0 : 360.0,
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        border: Border.all(color: Colors.grey.shade200),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 5.0),
            child: Text(
              'Búsqueda avanzada',
              style: Theme.of(context).textTheme.headline5,
            ),
          ),
          Expanded(
            child: LayoutBuilder(
              builder: (_, constraints) => constraints.maxWidth >= 425
                  ? AdvancedSearchBodyDesktop(ctrl: ctrl)
                  : AdvancedSearchBodyMobile(ctrl: ctrl),
            ),
          ),
        ],
      ),
    );
  }
}

class AdvancedSearchBodyDesktop extends StatelessWidget {
  const AdvancedSearchBodyDesktop({
    Key? key,
    required this.ctrl,
  }) : super(key: key);

  final IecsController ctrl;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Flexible(
              flex: 8,
              child: FormGroup(
                label: Text('Fecha desde'),
                input: AppInputDate(
                  key: ObjectKey(ctrl.startDate),
                  initialValue: ctrl.startDate,
                  onChanged: (DateTime? v) => ctrl.setStartDate(v),
                ),
              ),
            ),
            Spacer(),
            Flexible(
              flex: 8,
              child: FormGroup(
                label: Text('Fecha hasta'),
                input: AppInputDate(
                  key: ObjectKey(ctrl.endDate),
                  initialValue: ctrl.endDate,
                  onChanged: (DateTime? v) => ctrl.setEndDate(v),
                ),
              ),
            ),
            Spacer(),
            Flexible(
              flex: 7,
              child: FormGroup(
                label: Text('Nombre'),
                input: AppInput(
                  controller: ctrl.nameController,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 16),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Flexible(
              flex: 7,
              child: FormGroup(
                label: Text('Nº de Identificación'),
                input: AppInput(
                  controller: ctrl.identificationController,
                ),
              ),
            ),
            Spacer(),
            Flexible(
              flex: 7,
              child: FormGroup(
                label: Text('Solo efectivos'),
                input: AppSelect<bool>(
                  value: ctrl.onlyEffectives,
                  onChanged: (v) => ctrl.setOnlyEffectives(v),
                  items: [
                    {'id': null, 'name': 'Todos'},
                    {'id': true, 'name': 'Sí'},
                    {'id': false, 'name': 'No'},
                  ],
                ),
              ),
            ),
            Spacer(),
            Flexible(
              flex: 4,
              child: ButtonPrimary(
                child: Text('Buscar'),
                onPressed: () => ctrl.fetchIecs(),
              ),
            ),
            Spacer(),
            Flexible(
              flex: 4,
              child: ButtonWarning(
                child: Text('Limpiar'),
                onPressed: ctrl.clear,
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class AdvancedSearchBodyMobile extends StatelessWidget {
  const AdvancedSearchBodyMobile({
    Key? key,
    required this.ctrl,
  }) : super(key: key);

  final IecsController ctrl;

  @override
  Widget build(BuildContext context) {
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        FormGroup(
          label: Text('Fecha desde'),
          input: AppInputDate(
            key: ObjectKey(ctrl.startDate),
            initialValue: ctrl.startDate,
            onChanged: (DateTime? v) => ctrl.setStartDate(v),
          ),
        ),
        SizedBox(height: 5.0),
        FormGroup(
          label: Text('Fecha hasta'),
          input: AppInputDate(
            key: ObjectKey(ctrl.endDate),
            initialValue: ctrl.endDate,
            onChanged: (DateTime? v) => ctrl.setEndDate(v),
          ),
        ),
        SizedBox(height: 5.0),
        FormGroup(
          label: Text('Nombre'),
          input: AppInput(
            controller: ctrl.nameController,
          ),
        ),
        SizedBox(height: 5.0),
        FormGroup(
          label: Text('Nº de Identificación'),
          input: AppInput(
            controller: ctrl.identificationController,
          ),
        ),
        SizedBox(height: 5.0),
        FormGroup(
          label: Text('Solo efectivos'),
          input: AppSelect<bool>(
            value: ctrl.onlyEffectives,
            onChanged: (v) => ctrl.setOnlyEffectives(v),
            items: [
              {'id': null, 'name': 'Todos'},
              {'id': true, 'name': 'Sí'},
              {'id': false, 'name': 'No'},
            ],
          ),
        ),
        SizedBox(height: 5.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ButtonPrimary(
              child: Text('Buscar'),
              onPressed: () => ctrl.fetchIecs(),
            ),
            SizedBox(width: 5.0),
            ButtonWarning(
              child: Text('Limpiar'),
              onPressed: ctrl.clear,
            ),
          ],
        )
      ],
    );
  }
}
