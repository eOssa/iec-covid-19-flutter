import 'package:flutter/material.dart';
import 'package:flutter_meedu/router.dart' as router;

class WelcomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[100],
      padding: EdgeInsets.symmetric(horizontal: 32.0),
      width: double.infinity,
      child: LayoutBuilder(
        builder: (_, constraints) => constraints.maxWidth >= 768
            ? _WelcomeBodyDesktop()
            : _WelcomeBodyMobile(),
      ),
    );
  }
}

class _WelcomeBodyDesktop extends StatelessWidget {
  const _WelcomeBodyDesktop({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          width: 646,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Image(
              image: AssetImage('images/logo.jpeg'),
              height: 300.0,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 32),
          width: 646,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 2,
              ),
            ],
          ),
          child: Row(
            children: [
              Flexible(
                flex: 3,
                child: _WelcomeBox(
                  title: 'Lista de IECs',
                  description: 'Ver el listado de IECs creadas.',
                  icon: Icons.list,
                  onPressed: () {
                    router.pushNamed('/iecs');
                  },
                ),
              ),
              Flexible(
                flex: 3,
                child: _WelcomeBox(
                  title: 'Nueva IEC',
                  description: 'Agregar una nueva IEC.',
                  icon: Icons.add_circle_outlined,
                  withBorder: true,
                  onPressed: () {
                    router.pushNamed('/create-iec');
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _WelcomeBodyMobile extends StatelessWidget {
  const _WelcomeBodyMobile({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: 284,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Image(
              image: AssetImage('images/logo.jpeg'),
              height: 200.0,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 32),
          width: 284,
          height: 240,
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 2,
              ),
            ],
          ),
          child: Column(
            children: [
              Flexible(
                flex: 3,
                child: _WelcomeBox(
                  title: 'Lista de IECs',
                  description: 'Ver el listado de IECs creadas.',
                  icon: Icons.list,
                  onPressed: () {
                    router.pushNamed('/iecs');
                  },
                ),
              ),
              Flexible(
                flex: 3,
                child: _WelcomeBox(
                  title: 'Nueva IEC',
                  description: 'Agregar una nueva IEC.',
                  icon: Icons.add_circle_outlined,
                  withBorder: true,
                  forMobile: true,
                  onPressed: () {
                    router.pushNamed('/create-iec');
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _WelcomeBox extends StatelessWidget {
  _WelcomeBox({
    this.title: '',
    this.description: '',
    this.icon: Icons.book,
    this.withBorder: false,
    this.forMobile: false,
    required this.onPressed,
  });
  final String title;
  final String description;
  final IconData icon;
  final bool withBorder;
  final bool forMobile;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24.0),
      decoration: BoxDecoration(
        border: withBorder
            ? Border(
                left: forMobile
                    ? BorderSide.none
                    : BorderSide(width: 1, color: Colors.grey.shade200),
                top: forMobile
                    ? BorderSide(width: 1, color: Colors.grey.shade200)
                    : BorderSide.none,
              )
            : null,
      ),
      child: Column(
        children: [
          Row(
            children: [
              Flexible(
                flex: 1,
                child: Icon(
                  icon,
                  color: Colors.grey,
                  size: 32.0,
                ),
              ),
              Flexible(
                flex: 5,
                child: Container(
                  margin: EdgeInsets.only(left: 16.0),
                  child: TextButton(
                    child: Text(
                      title,
                      style: TextStyle(
                        height: 1.0,
                        fontSize: 18.0,
                        fontWeight: FontWeight.w600,
                        color: Colors.grey[900],
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    onPressed: onPressed,
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 48.0, top: 8.0),
            alignment: Alignment.centerLeft,
            child: Text(
              description,
              style: TextStyle(
                color: Colors.grey[600],
                fontSize: 14.0,
                height: 1.0,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
