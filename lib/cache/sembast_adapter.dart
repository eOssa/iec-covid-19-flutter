import 'package:iec_covid_19_flutter/cache/cache_interface.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

class SembastAdapter implements CacheInterface {
  late final Database _db;
  final String _cacheName = 'iec-covid-19.cache';
  final StoreRef store = StoreRef.main();

  SembastAdapter() {
    open().then((db) => _db = db);
  }

  @override
  Future<bool> forget(String key) async {
    await store.record(key).delete(_db);
    return true;
  }

  @override
  Future<V> get<V>(String key) async {
    return await store.record(key).get(_db) as V;
  }

  @override
  Future<bool> has(String key) async {
    return await get(key) != null;
  }

  @override
  Future<bool> put(String key, value) async {
    await store.record(key).put(_db, value);
    return true;
  }

  Future<Database> open() async {
    final dir = await getApplicationDocumentsDirectory();
    return await databaseFactoryIo.openDatabase(join(dir.path, _cacheName));
  }
}
