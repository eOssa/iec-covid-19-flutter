abstract class CacheInterface {
  Future<bool> forget(String key);

  Future<V> get<V>(String key);

  Future<bool> has(String key);

  Future<bool> put(String key, dynamic value);
}
