import 'package:iec_covid_19_flutter/models/medication_model.dart';
import 'package:iec_covid_19_flutter/repositories/medication_repository_interface.dart';
import 'package:meedu/meedu.dart';

class CreateMedicationController extends SimpleNotifier {
  String name;

  CreateMedicationController({this.name: ''});

  Future<MedicationModel> store() async {
    MedicationModel medication =
        await Get.i.find<MedicationRepositoryInterface>().store(name);
    name = '';
    notify();
    return medication;
  }
}
