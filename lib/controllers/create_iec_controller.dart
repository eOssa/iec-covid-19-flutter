import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/cache/cache_interface.dart';
import 'package:iec_covid_19_flutter/helpers/date.dart';
import 'package:iec_covid_19_flutter/models/contact_model.dart';
import 'package:iec_covid_19_flutter/models/iec_model.dart';
import 'package:iec_covid_19_flutter/repositories/comorbidity_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/exam_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/iec_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/medication_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/symptom_repository_interface.dart';
import 'package:meedu/meedu.dart';

class CreateIecController extends SimpleNotifier {
  static String defaultNursingNote =
      'Usuari@ de __ años de edad, con antecedentes patológicos de ___, '
      'paciente con reporte positivo del día __ de Noviembre, se encuentra '
      'en domicilio, al día de hoy refiere que "inició síntomas el día '
      '________, presentó: fiebre de 38.9º, dificultad respiratoria, dolor '
      'torácico, dolor de cabeza y malestar general, es independiente tiene '
      'su propio negocio, convive actualmente con su esposo e hijos, todos '
      'ellos presentan síntomas, niega viajes nacionales e internacionales, '
      'no frecuenta ningún lugar, pero su esposo si, el supermercado y el '
      'banco dice que allí puede haberse contagiado. Se le realizó la toma '
      'de la prueba el día ________ en el Laboratorio López Correa, '
      'Antígeno, le formularon acetaminofén, y se está realizando remedios '
      'caseros con moringa".';
  IecModel _iec = IecModel(
    contact: ContactModel(
      address: '',
      cityId: 832,
      comorbidities: [],
      displacements: [],
      eps: '',
      firstName: '',
      genderId: 1,
      hospitalizations: [],
      identification: '',
      identificationTypeId: 1,
      lastName: '',
      middleName: '',
      nationality: 'Colombiano',
      primaryPhone: '',
      pregnancyWeeks: 0,
      regionId: 26,
      relationship: '',
      symptoms: [],
      zoneId: 2,
    ),
    contacts: [],
    date: DateTime.now(),
    effective: true,
    examId: 1,
    medications: [],
    nursingNote: defaultNursingNote,
    sampleTypeId: 2,
    withPositiveContact: false,
  );
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  String _cacheKey = 'iecsDraft';
  bool existsDraft = false;
  TextEditingController nursingNoteController =
      TextEditingController(text: defaultNursingNote);
  bool customNursingNote = false;
  bool disabledIecsReport = false;
  bool disabledContactsIecReport = false;

  String? get address => _iec.contact?.address;
  int get age {
    if (_iec.contact?.birthdate == null) {
      return 0;
    }
    DateTime now = DateTime.now();
    double diffInDays = now.difference(_iec.contact!.birthdate!).inDays / 365;
    return diffInDays.floor().abs();
  }

  DateTime? get birthdate => _iec.contact?.birthdate;
  int? get cityId => _iec.contact?.cityId;
  List<int> get comorbidities => _iec.contact!.comorbidities;
  List<ContactModel> get contacts => _iec.contacts!;
  DateTime? get date => _iec.date;
  List<Map<String, dynamic>> get displacements => _iec.contact!.displacements!;
  bool get effective => _iec.effective!;
  String? get eps => _iec.contact?.eps;
  int get examId => _iec.examId!;
  String? get firstName => _iec.contact?.firstName;
  int get gender => _iec.contact!.genderId;
  List<Map<String, dynamic>> get hospitalizations =>
      _iec.contact!.hospitalizations!;
  IecModel? get iec => _iec;
  String? get identification => _iec.contact?.identification;
  int get identificationTypeId => _iec.contact!.identificationTypeId;
  bool get isEdit => _iec.id != 0;
  List<int> get medications => _iec.medications!;
  String? get lastName => _iec.contact?.lastName;
  String? get middleName => _iec.contact?.middleName;
  String? get nationality => _iec.contact?.nationality;
  String? get neighborhood => _iec.contact?.neighborhood;
  String? get occupation => _iec.contact?.occupation;
  String? get primaryPhone => _iec.contact?.primaryPhone;
  String? get positiveContactPlace => _iec.positiveContactPlace;
  int get pregnancyWeeks => _iec.contact!.pregnancyWeeks!;
  int? get regionId => _iec.contact?.regionId;
  DateTime? get sampleDate => _iec.sampleDate;
  String? get sampleLaboratory => _iec.sampleLaboratory;
  int get sampleTypeId => _iec.sampleTypeId!;
  String? get secondaryPhone => _iec.contact?.secondaryPhone;
  String? get secondLastName => _iec.contact?.secondLastName;
  DateTime? get symptomDate => _iec.symptomDate;
  List<int> get symptoms => _iec.contact!.symptoms;
  bool get withPositiveContact => _iec.withPositiveContact!;
  int? get zone => _iec.contact?.zoneId;

  CreateIecController(int id) {
    if (id != 0) {
      edit(id);
    } else {
      checkDraft();
    }
  }

  void addContact(ContactModel contact) {
    _iec.contacts?.add(contact);
    putIecInDraft();
    notify();
  }

  void addComorbidity(int comorbidity) {
    _iec.contact?.comorbidities.add(comorbidity);
    putIecInDraft();
    notify();
  }

  void addDisplacement(Map<String, dynamic> displacement) {
    _iec.contact?.displacements?.add(displacement);
    putIecInDraft();
    notify();
  }

  void addHospitalization(Map<String, dynamic> hospitalization) {
    _iec.contact?.hospitalizations?.add(hospitalization);
    putIecInDraft();
    notify();
  }

  void addMedication(int medication) {
    _iec.medications?.add(medication);
    putIecInDraft();
    notify();
  }

  void addSymptom(int symptom) {
    _iec.contact?.symptoms.add(symptom);
    putIecInDraft();
    notify();
  }

  void checkDraft() async {
    existsDraft = await Get.i.find<CacheInterface>().has(_cacheKey);
    if (existsDraft) {
      notify();
    }
  }

  void deleteContact(ContactModel contact) {
    _iec.contacts?.remove(contact);
    putIecInDraft();
    suggestNursingNote();
    notify();
  }

  void deleteDisplacement(id) {
    _iec.contact?.displacements?.remove(findDisplacement(id));
    putIecInDraft();
    notify();
  }

  void deleteDraft() async {
    await Get.i.find<CacheInterface>().forget(_cacheKey);
    existsDraft = false;
    notify();
  }

  void deleteHospitalization(id) {
    _iec.contact?.hospitalizations?.remove(findHospitalization(id));
    putIecInDraft();
    notify();
  }

  Future edit(int id) async {
    _iec = await Get.i.find<IecRepositoryInterface>().showIec(id);
    nursingNoteController.text = _iec.nursingNote ?? defaultNursingNote;
    customNursingNote = _iec.nursingNote != defaultNursingNote;
    notify();
  }

  Map<String, dynamic> findDisplacement(id) {
    return displacements.firstWhere((item) => item['id'] == id);
  }

  Map<String, dynamic> findHospitalization(id) {
    return hospitalizations.firstWhere((item) => item['id'] == id);
  }

  void loadDraft() async {
    _iec = IecModel.fromJson(
      await Get.i.find<CacheInterface>().get<Map<String, dynamic>>(_cacheKey),
    );
    nursingNoteController.text = _iec.nursingNote ?? defaultNursingNote;
    existsDraft = false;
    notify();
  }

  void putIecInDraft() {
    _iec.nursingNote = nursingNoteController.text;
    if (!isEdit) {
      Get.i.find<CacheInterface>().put(_cacheKey, _iec.toMap());
    }
  }

  void suggestNursingNote() async {
    if (customNursingNote) {
      return null;
    }
    var note = StringBuffer();
    note.write('Usuari${gender == 1 ? "a" : "o"} de ');
    note.write('$age año${age == 1 ? "" : "s"} de edad, ');
    if (comorbidities.isNotEmpty) {
      var names = await Get.i
          .find<ComorbidityRepositoryInterface>()
          .getNames(comorbidities);
      note.write('con antecedentes patológicos de ${names.join(', ')}, ');
    }
    var symDate = DateHelper.format(symptomDate, format: "d 'de' MMMM");
    note.write('paciente con reporte positivo del día $symDate, ');
    note.write('se encuentra en domicilio, ');
    note.write('al día de hoy refiere que ');
    note.write('"inició síntomas el día $symDate, ');
    if (symptoms.isNotEmpty) {
      var names =
          await Get.i.find<SymptomRepositoryInterface>().getNames(symptoms);
      note.write('presentó: ${names.join(', ')}, ');
    }
    if (hospitalizations.isNotEmpty) {
      hospitalizations.forEach((value) {
        var date = DateHelper.format(value['date'], format: "d 'de' MMMM");
        note.write('fue hospitalizad${gender == 1 ? "a" : "o"} el $date ');
        note.write('en ${value["institution"]}, ');
      });
    }
    if (occupation != null) {
      note.write('es $occupation, ');
    }
    if (contacts.isNotEmpty) {
      var relationships = contacts.map((e) => e.relationship).join(', su ');
      note.write('convive actualmente con su $relationships, ');
      var contactsWithSymptoms = contacts.where((v) => v.symptoms.isNotEmpty);
      if (contactsWithSymptoms.isNotEmpty) {
        if (contacts.length == 1) {
          note.write('quien presenta síntomas, ');
        } else if (contactsWithSymptoms.length == contacts.length) {
          note.write('todos ellos presentan síntomas, ');
        } else {
          contactsWithSymptoms.forEach((value) {
            note.write('su ${value.relationship} presenta síntomas, ');
          });
        }
      }
    }
    if (displacements.isEmpty) {
      note.write('niega viajes nacionales e internacionales, ');
    } else {
      displacements.forEach((value) {
        var start =
            DateHelper.format(value['startDate'], format: "d 'de' MMMM");
        var end = DateHelper.format(value['endDate'], format: "d 'de' MMMM");
        note.write('viajó a ${value["city"]} del $start al $end, ');
      });
    }
    if (positiveContactPlace != null) {
      note.write('pudo haberse contagiado en $positiveContactPlace. ');
    }
    var samDate = DateHelper.format(sampleDate, format: "d 'de' MMMM");
    note.write('Se le realizó la toma de la prueba el día $samDate ');
    var examName = await Get.i.find<ExamRepositoryInterface>().findName(examId);
    note.write('en $sampleLaboratory, $examName, ');
    if (medications.isNotEmpty) {
      var names = await Get.i
          .find<MedicationRepositoryInterface>()
          .getNames(medications);
      note.write('le formularon ${names.join(", ")}');
    }
    note.write('".');
    nursingNoteController.text = note.toString();
  }

  void setAddress(String? address) {
    _iec.contact?.address = address ?? '';
    putIecInDraft();
  }

  void setBirthdate(DateTime? birthdate) {
    _iec.contact?.birthdate = birthdate;
    putIecInDraft();
    suggestNursingNote();
    notify();
  }

  void setCityId(int? cityId) {
    _iec.contact?.cityId = cityId;
    putIecInDraft();
    notify();
  }

  void setComorbidities(List<int> comorbidities) {
    _iec.contact?.comorbidities = comorbidities;
    putIecInDraft();
    suggestNursingNote();
    notify();
  }

  void setDate(DateTime? date) {
    _iec.date = date;
    putIecInDraft();
  }

  void setDisabledContactsIecReport(bool value) {
    disabledContactsIecReport = value;
    notify();
  }

  void setDisabledIecsReport(bool value) {
    disabledIecsReport = value;
    notify();
  }

  void setEffective(bool? effective) {
    _iec.effective = effective;
    _iec.contact?.nationality = effective! ? 'Colombiano' : null;
    nursingNoteController.text = effective! ? defaultNursingNote : '';
    customNursingNote = _iec.nursingNote != defaultNursingNote;
    putIecInDraft();
    notify();
  }

  void setEps(String? eps) {
    _iec.contact?.eps = eps ?? '';
    putIecInDraft();
  }

  void setExamId(int examId) {
    _iec.examId = examId;
    putIecInDraft();
    suggestNursingNote();
  }

  void setFirstName(String? firstName) {
    _iec.contact?.firstName = firstName ?? '';
    putIecInDraft();
  }

  void setGender(int genderId) {
    _iec.contact?.genderId = genderId;
    putIecInDraft();
    suggestNursingNote();
    notify();
  }

  void setIdentification(String? identification) {
    _iec.contact?.identification = identification ?? '';
    putIecInDraft();
  }

  void setIdentificationTypeId(int identificationTypeId) {
    _iec.contact?.identificationTypeId = identificationTypeId;
    putIecInDraft();
    notify();
  }

  void setLastName(String? lastName) {
    _iec.contact?.lastName = lastName ?? '';
    putIecInDraft();
  }

  void setMedications(List<int> medications) {
    _iec.medications = medications;
    putIecInDraft();
    suggestNursingNote();
    notify();
  }

  void setMiddleName(String? middleName) {
    _iec.contact?.middleName = middleName;
    putIecInDraft();
  }

  void setNationality(String? nationality) {
    _iec.contact?.nationality = nationality;
    putIecInDraft();
  }

  void setNeighborhood(String? neighborhood) {
    _iec.contact?.neighborhood = neighborhood;
    putIecInDraft();
  }

  void nursingNoteOnChange() {
    customNursingNote = true;
    putIecInDraft();
  }

  void setOccupation(String? occupation) {
    _iec.contact?.occupation = occupation;
    putIecInDraft();
    suggestNursingNote();
  }

  void setPrimaryPhone(String? primaryPhone) {
    _iec.contact?.primaryPhone = primaryPhone ?? '';
    putIecInDraft();
  }

  void setPositiveContactPlace(String? positiveContactPlace) {
    _iec.positiveContactPlace = positiveContactPlace;
    putIecInDraft();
    suggestNursingNote();
  }

  void setPregnancyWeeks(int? pregnancyWeeks) {
    _iec.contact?.pregnancyWeeks = pregnancyWeeks;
    putIecInDraft();
  }

  void setRegionId(int? regionId) {
    _iec.contact?.regionId = regionId;
    putIecInDraft();
    notify();
  }

  void setSampleDate(DateTime? sampleDate) {
    _iec.sampleDate = sampleDate;
    putIecInDraft();
    suggestNursingNote();
  }

  void setSampleLaboratory(String sampleLaboratory) {
    _iec.sampleLaboratory = sampleLaboratory;
    putIecInDraft();
    suggestNursingNote();
  }

  void setSampleTypeId(int sampleTypeId) {
    _iec.sampleTypeId = sampleTypeId;
    putIecInDraft();
  }

  void setSecondaryPhone(String? secondaryPhone) {
    _iec.contact?.secondaryPhone = secondaryPhone;
    putIecInDraft();
  }

  void setSecondLastName(String? secondLastName) {
    _iec.contact?.secondLastName = secondLastName;
    putIecInDraft();
  }

  void setSymptomDate(DateTime? symptomDate) {
    _iec.symptomDate = symptomDate;
    putIecInDraft();
    suggestNursingNote();
  }

  void setSymptoms(List<int> symptoms) {
    _iec.contact?.symptoms = symptoms;
    putIecInDraft();
    suggestNursingNote();
  }

  void setWithPositiveContact(bool? withPositiveContact) {
    _iec.withPositiveContact = withPositiveContact;
    putIecInDraft();
    notify();
  }

  void setZone(int zoneId) {
    _iec.contact?.zoneId = zoneId;
    putIecInDraft();
    notify();
  }

  Future storeIec() async {
    _iec.id = await Get.i.find<IecRepositoryInterface>().store(_iec);
    Get.i.find<CacheInterface>().forget(_cacheKey);
  }

  void updateContact(ContactModel contact) {
    _iec.contacts = _iec.contacts?.map((item) {
      return item == contact ? contact : item;
    }).toList();
    putIecInDraft();
    suggestNursingNote();
    notify();
  }

  void updateDisplacement(Map<String, dynamic> displacement) {
    _iec.contact?.displacements =
        _iec.contact?.displacements?.map((Map<String, dynamic> item) {
      if (displacement['id'] == item['id']) {
        return displacement;
      }
      return item;
    }).toList();
    putIecInDraft();
    suggestNursingNote();
    notify();
  }

  void updateDisplacementCity(id, String city) {
    Map<String, dynamic> displacement = findDisplacement(id);
    displacement['city'] = city;
    updateDisplacement(displacement);
  }

  void updateDisplacementCountry(id, String country) {
    Map<String, dynamic> displacement = findDisplacement(id);
    displacement['country'] = country;
    updateDisplacement(displacement);
  }

  void updateDisplacementEndDate(id, DateTime? endDate) {
    Map<String, dynamic> displacement = findDisplacement(id);
    displacement['endDate'] = endDate;
    updateDisplacement(displacement);
  }

  void updateDisplacementStartDate(id, DateTime? startDate) {
    Map<String, dynamic> displacement = findDisplacement(id);
    displacement['startDate'] = startDate;
    updateDisplacement(displacement);
  }

  void updateHospitalization(Map<String, dynamic> hospitalization) {
    _iec.contact?.hospitalizations =
        _iec.contact?.hospitalizations?.map((Map<String, dynamic> item) {
      if (hospitalization['id'] == item['id']) {
        return hospitalization;
      }
      return item;
    }).toList();
    putIecInDraft();
    notify();
  }

  void updateHospitalizationDate(id, DateTime? date) {
    Map<String, dynamic> hospitalization = findHospitalization(id);
    hospitalization['date'] = date;
    updateHospitalization(hospitalization);
  }

  void updateHospitalizationInstitution(id, String institution) {
    Map<String, dynamic> hospitalization = findHospitalization(id);
    hospitalization['institution'] = institution;
    updateHospitalization(hospitalization);
  }
}
