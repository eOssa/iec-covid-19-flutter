import 'package:iec_covid_19_flutter/models/zone_model.dart';
import 'package:iec_covid_19_flutter/repositories/zone_repository_interface.dart';
import 'package:meedu/meedu.dart';

class ZonesController extends SimpleNotifier {
  List<ZoneModel> zones = [];

  ZonesController() {
    fetchAll();
  }

  void fetchAll() async {
    zones = await Get.i.find<ZoneRepositoryInterface>().all();
    if (!disposed) {
      notify();
    }
  }
}
