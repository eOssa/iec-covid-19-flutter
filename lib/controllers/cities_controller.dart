import 'package:iec_covid_19_flutter/models/city_model.dart';
import 'package:iec_covid_19_flutter/repositories/city_repository_interface.dart';
import 'package:meedu/meedu.dart';

class CitiesController extends SimpleNotifier {
  List<CityModel> cities = [];
  final int regionId;

  CitiesController(this.regionId) {
    fetchByRegionId();
  }

  void fetchByRegionId() async {
    cities = await Get.i.find<CityRepositoryInterface>().getByRegion(regionId);
    if (!disposed) {
      notify();
    }
  }
}
