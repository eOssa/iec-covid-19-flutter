import 'package:iec_covid_19_flutter/models/symptom_model.dart';
import 'package:iec_covid_19_flutter/repositories/symptom_repository_interface.dart';
import 'package:meedu/meedu.dart';

class SymptomsController extends SimpleNotifier {
  List<SymptomModel> symptoms = [];

  SymptomsController() {
    fetchAll();
  }

  void fetchAll() async {
    symptoms = await Get.i.find<SymptomRepositoryInterface>().all();
    if (!disposed) {
      notify();
    }
  }
}
