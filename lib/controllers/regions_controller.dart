import 'package:iec_covid_19_flutter/models/region_model.dart';
import 'package:iec_covid_19_flutter/repositories/region_repository_interface.dart';
import 'package:meedu/meedu.dart';

class RegionsController extends SimpleNotifier {
  List<RegionModel> regions = [];

  RegionsController() {
    fetchAll();
  }

  void fetchAll() async {
    regions = await Get.i.find<RegionRepositoryInterface>().all();
    if (!disposed) {
      notify();
    }
  }
}
