import 'package:iec_covid_19_flutter/models/identification_type_model.dart';
import 'package:iec_covid_19_flutter/repositories/identification_type_repository_interface.dart';
import 'package:meedu/meedu.dart';

class IdentificationsTypesController extends SimpleNotifier {
  List<IdentificationTypeModel> values = [];

  IdentificationsTypesController() {
    fetchAll();
  }

  void fetchAll() async {
    values = await Get.i.find<IdentificationTypeRepositoryInterface>().all();
    if (!disposed) {
      notify();
    }
  }
}
