import 'package:iec_covid_19_flutter/models/symptom_model.dart';
import 'package:iec_covid_19_flutter/repositories/symptom_repository_interface.dart';
import 'package:meedu/meedu.dart';

class CreateSymptomController extends SimpleNotifier {
  String name;

  CreateSymptomController({this.name: ''});

  Future<SymptomModel> store() async {
    SymptomModel symptom =
        await Get.i.find<SymptomRepositoryInterface>().store(name);
    name = '';
    notify();
    return symptom;
  }
}
