import 'dart:io';

import 'package:iec_covid_19_flutter/models/iec_model.dart';
import 'package:iec_covid_19_flutter/repositories/iecs_report_repository_interface.dart';
import 'package:meedu/meedu.dart';
import 'package:path_provider/path_provider.dart';

class IecsReportController extends SimpleNotifier {
  Future generate(List<IecModel> iecs) async {
    final repository = Get.i.find<IecsReportRepositoryInterface>();
    final res = await repository.generate(iecs);
    final String filename = res.keys.first;
    final List<int> bytes = res[filename]!;
    String path = '/storage/emulated/0/Download';
    if (!Platform.isAndroid) {
      final Directory? dir = await getDownloadsDirectory();
      if (dir == null) {
        return null;
      }
      path = dir.path;
    }
    final File file = new File('$path/$filename');
    await file.writeAsBytes(bytes);
  }
}
