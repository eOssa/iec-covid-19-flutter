import 'package:iec_covid_19_flutter/models/medication_model.dart';
import 'package:iec_covid_19_flutter/repositories/medication_repository_interface.dart';
import 'package:meedu/meedu.dart';

class MedicationsController extends SimpleNotifier {
  List<MedicationModel> medications = [];

  MedicationsController() {
    fetchAll();
  }

  void fetchAll() async {
    medications = await Get.i.find<MedicationRepositoryInterface>().all();
    notify();
  }
}
