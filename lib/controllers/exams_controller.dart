import 'package:iec_covid_19_flutter/models/exam_model.dart';
import 'package:iec_covid_19_flutter/repositories/exam_repository_interface.dart';
import 'package:meedu/meedu.dart';

class ExamsController extends SimpleNotifier {
  List<ExamModel> exams = [];

  ExamsController() {
    fetchAll();
  }

  void fetchAll() async {
    exams = await Get.i.find<ExamRepositoryInterface>().all();
    if (!disposed) {
      notify();
    }
  }
}
