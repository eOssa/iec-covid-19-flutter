import 'package:iec_covid_19_flutter/models/comorbidity_model.dart';
import 'package:iec_covid_19_flutter/repositories/comorbidity_repository_interface.dart';
import 'package:meedu/meedu.dart';

class CreateComorbidityController extends SimpleNotifier {
  String name;

  CreateComorbidityController({this.name: ''});

  Future<ComorbidityModel> store() async {
    ComorbidityModel comorbidity =
        await Get.i.find<ComorbidityRepositoryInterface>().store(name);
    name = '';
    notify();
    return comorbidity;
  }
}
