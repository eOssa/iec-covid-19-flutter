import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/models/iec_model.dart';
import 'package:iec_covid_19_flutter/repositories/iec_repository_interface.dart';
import 'package:meedu/meedu.dart';

class IecsController extends SimpleNotifier {
  List<IecModel> iecs = [];
  List<Map<String, dynamic>> iecsMaps = [];
  DateTime? endDate;
  DateTime? startDate;
  bool? onlyEffectives;
  TextEditingController identificationController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  int total = 0;
  int perPage = 10;
  int _page = 1;

  IecsController() {
    fetchIecs();
  }

  void clear() {
    endDate = null;
    startDate = null;
    identificationController.text = '';
    onlyEffectives = null;
    nameController.text = '';
    notify();
    fetchIecs();
  }

  void deleteIec(Map<String, dynamic> iec) {
    IecRepositoryInterface repository = Get.i.find<IecRepositoryInterface>();
    repository.deleteIec(iec['id']);
    iecsMaps.remove(iec);
    total--;
    notify();
  }

  void fetchIecs() async {
    IecRepositoryInterface repository = Get.i.find<IecRepositoryInterface>();
    List<IecModel> data = await repository.getIecs(
      perPage: perPage,
      page: _page,
      endDate: endDate,
      identification: identificationController.text,
      name: nameController.text,
      onlyEffectives: onlyEffectives,
      startDate: startDate,
    );
    iecs = data;
    iecsMaps = data.map<Map<String, dynamic>>((IecModel iec) {
      Map<String, dynamic> iecMap = iec.toMap();
      iecMap['name'] = iec.contact!.name;
      iecMap['identification'] = iec.contact!.identification;
      iecMap['effective'] = iec.effective! ? 'Sí' : 'No';
      return iecMap;
    }).toList();
    await totalIecs();
    notify();
  }

  void setEndDate(DateTime? value) {
    endDate = value;
    notify();
  }

  void setOnlyEffectives(bool? value) {
    onlyEffectives = value;
    notify();
  }

  void setStartDate(DateTime? value) {
    startDate = value;
    notify();
  }

  Future totalIecs() async {
    IecRepositoryInterface repository = Get.i.find<IecRepositoryInterface>();
    total = await repository.total(
      endDate: endDate,
      identification: identificationController.text,
      name: nameController.text,
      onlyEffectives: onlyEffectives,
      startDate: startDate,
    );
  }

  void updatePerPage(int value) {
    perPage = value;
    fetchIecs();
  }

  void updatePage(int value) {
    _page = ((value + perPage) / perPage).round();
    fetchIecs();
  }
}
