import 'package:iec_covid_19_flutter/models/comorbidity_model.dart';
import 'package:iec_covid_19_flutter/repositories/comorbidity_repository_interface.dart';
import 'package:meedu/meedu.dart';

class ComorbiditiesController extends SimpleNotifier {
  List<ComorbidityModel> comorbidities = [];

  ComorbiditiesController() {
    fetchAll();
  }

  void fetchAll() async {
    comorbidities = await Get.i.find<ComorbidityRepositoryInterface>().all();
    if (!disposed) {
      notify();
    }
  }
}
