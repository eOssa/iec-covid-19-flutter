import 'package:iec_covid_19_flutter/models/sample_type_model.dart';
import 'package:iec_covid_19_flutter/repositories/sample_type_repository_interface.dart';
import 'package:meedu/meedu.dart';

class SamplesTypesController extends SimpleNotifier {
  List<SampleTypeModel> samplesTypes = [];

  SamplesTypesController() {
    fetchAll();
  }

  void fetchAll() async {
    samplesTypes = await Get.i.find<SampleTypeRepositoryInterface>().all();
    if (!disposed) {
      notify();
    }
  }
}
