import 'package:iec_covid_19_flutter/models/gender_model.dart';
import 'package:iec_covid_19_flutter/repositories/gender_repository_interface.dart';
import 'package:meedu/meedu.dart';

class GendersController extends SimpleNotifier {
  List<GenderModel> genders = [];

  GendersController() {
    fetchAll();
  }

  void fetchAll() async {
    genders = await Get.i.find<GenderRepositoryInterface>().all();
    if (!disposed) {
      notify();
    }
  }
}
