import 'package:flutter/material.dart';

import 'package:iec_covid_19_flutter/ui/views/create_iec_view.dart';
import 'package:iec_covid_19_flutter/ui/views/iecs_view.dart';
import 'package:iec_covid_19_flutter/ui/views/view_404.dart';
import 'package:iec_covid_19_flutter/ui/views/welcome_view.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return _fadeRoute(WelcomeView(), '/');
      case '/iecs':
        return _fadeRoute(IecsView(), '/iecs');
      case '/create-iec':
        final id = settings.arguments as int?;
        return _fadeRoute(CreateIecView(id: id ?? 0), '/create-iec');
      default:
        return _fadeRoute(View404(), '/404');
    }
  }

  static PageRoute _fadeRoute(Widget child, String routeName) {
    return PageRouteBuilder(
      settings: RouteSettings(name: routeName),
      pageBuilder: (_, __, ___) => child,
      transitionsBuilder: (_, animation, __, ___) => FadeTransition(
        opacity: animation,
        child: child,
      ),
    );
  }
}
