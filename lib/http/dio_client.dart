import 'package:dio/dio.dart';
import 'package:iec_covid_19_flutter/http/client_interface.dart';

class DioClient extends HttpClientInterface {
  final Dio client;
  DioClient()
      : client = Dio(
          BaseOptions(
            headers: {'X-Requested-With': 'XMLHttpRequest'},
          ),
        );

  @override
  Future<dynamic> get(
    String path, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? options,
  }) {
    return client.get<List<int>>(
      path,
      queryParameters: queryParameters,
      options: parseOptions(options),
    );
  }

  @override
  Future<dynamic> post(String path, {data, Map<String, dynamic>? options}) {
    return client.post(path, data: data, options: parseOptions(options));
  }

  @override
  Future put(String path, {data, Map<String, dynamic>? options}) {
    return client.put(path, data: data, options: parseOptions(options));
  }

  Options parseOptions(Map<String, dynamic>? options) {
    Map<String, ResponseType> responseTypes = {
      'stream': ResponseType.stream,
      'bytes': ResponseType.bytes,
      'json': ResponseType.json,
      'plain': ResponseType.plain,
    };
    return Options(responseType: responseTypes[options?['response_type']]);
  }
}
