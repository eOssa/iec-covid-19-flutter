abstract class HttpClientInterface {
  Future<dynamic> get(
    String path, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? options,
  });
  Future<dynamic> post(String path, {data, Map<String, dynamic>? options});
  Future<dynamic> put(String path, {data, Map<String, dynamic>? options});
}
