import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:iec_covid_19_flutter/providers/app_service_provider.dart';
import 'package:flutter_meedu/router.dart' as router;
import 'package:iec_covid_19_flutter/router/route_generator.dart';
import 'package:iec_covid_19_flutter/ui/layout/main_layout_page.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  AppServiceProvider.register();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      key: router.appKey,
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [GlobalMaterialLocalizations.delegate],
      supportedLocales: [const Locale('en'), const Locale('es_CO')],
      title: "IEC Covid-19",
      navigatorKey: router.navigatorKey,
      initialRoute: '/',
      onGenerateRoute: RouteGenerator.generateRoute,
      builder: (_, child) => MainLayoutPage(child: child ?? Container()),

      // this allows to flutter_meedu listen the changes in your navigator
      navigatorObservers: [router.observer],
    );
  }
}
