import 'package:intl/intl.dart';

class DateHelper {
  static endOfDay(DateTime date) {
    return new DateTime(date.year, date.month, date.day, 23, 59, 59);
  }

  static format(DateTime? dateTime, {String format = 'yyyy-MM-dd'}) {
    if (dateTime == null) {
      return null;
    }
    return DateFormat(format, 'es').format(dateTime);
  }

  static parse(String? date) {
    if (date == null) {
      return null;
    }
    return DateTime.parse(date);
  }
}
