import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/identifications_types_controller.dart';
import 'package:iec_covid_19_flutter/widgets/app_select.dart';

final identificationsTypesProvider =
    SimpleProvider((ref) => IdentificationsTypesController());

class SelectIdentificationsTypes extends StatelessWidget {
  final int? value;
  final void Function(int)? onChanged;
  final bool isExpanded;
  final bool short;

  const SelectIdentificationsTypes({
    Key? key,
    this.value,
    this.onChanged,
    this.isExpanded: true,
    this.short: false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(identificationsTypesProvider);
        return AppSelect<int>(
          value: value,
          isExpanded: isExpanded,
          onChanged: (v) => onChanged!(v!),
          items: ctrl.values.map((value) {
            Map<String, dynamic> map = value.toMap();
            if (!short) {
              map['name'] = {
                1: 'CC - Cédula de ciudadanía',
                2: 'TI - Tarjeta de identidad',
                3: 'RC - Registro civil',
                4: 'CE - Cédula de extranjería',
                5: 'PA - Pasaporte',
                6: 'PP - Permiso de permanencia',
              }[value.id];
            }
            return map;
          }).toList(),
        );
      },
    );
  }
}
