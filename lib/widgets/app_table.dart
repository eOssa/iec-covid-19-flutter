import 'package:flutter/material.dart';

class AppTable extends StatelessWidget {
  final List<String> columns;
  final List<Map<String, dynamic>> data;
  final Map options;
  final int perPage;
  final int totalRecords;
  final Map<String, Function> customColumns;
  final DataTableSource _source;
  final void Function(int?)? onRowsPerPageChanged;
  final void Function(int)? onPageChanged;

  AppTable({
    Key? key,
    required this.columns,
    required this.data,
    this.options: const {},
    this.perPage: 10,
    this.totalRecords: 0,
    this.customColumns: const {},
    this.onRowsPerPageChanged,
    this.onPageChanged,
  }) : _source = AppTableDataSource(
          columns: columns,
          data: data,
          customColumns: customColumns,
          totalRecords: totalRecords,
        );

  String getHeading(String column) {
    if (options.containsKey('headings')) {
      if (options['headings'].containsKey(column)) {
        return options['headings'][column];
      }
    }
    return column.replaceAll('_', ' ');
  }

  List<DataColumn> getTableColumns() {
    List<DataColumn> dataColumns = [];
    columns.forEach((column) {
      dataColumns.add(
        DataColumn(
          label: Text(getHeading(column)),
        ),
      );
    });
    return dataColumns;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: PaginatedDataTable(
        columns: getTableColumns(),
        source: _source,
        rowsPerPage: perPage,
        onRowsPerPageChanged: onRowsPerPageChanged,
        onPageChanged: onPageChanged,
      ),
    );
  }
}

class AppTableDataSource extends DataTableSource {
  AppTableDataSource({
    required List<String> columns,
    required List<Map<String, dynamic>> data,
    required Map<String, Function> customColumns,
    required final int totalRecords,
  })   : _columns = columns,
        _data = data,
        _customColumns = customColumns,
        _totalRecords = totalRecords;
  final List<String> _columns;
  final List<Map<String, dynamic>> _data;
  final Map<String, Function> _customColumns;
  final int _totalRecords;

  Widget getCellWidget(item, key) {
    if (_customColumns.containsKey(key) && _customColumns[key] is Function) {
      return _customColumns[key]!(item);
    }
    return Text(item[key].toString());
  }

  List<DataCell> getDataRowCells(Map<String, dynamic> item) {
    List<DataCell> cells = [];
    _columns.forEach((column) {
      cells.add(DataCell(getCellWidget(item, column)));
    });
    return cells;
  }

  @override
  DataRow? getRow(int index) {
    if (!_data.asMap().containsKey(index)) {
      return null;
    }
    return DataRow.byIndex(
      index: index,
      cells: getDataRowCells(_data[index]),
    );
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _totalRecords;

  @override
  int get selectedRowCount => 0;
}
