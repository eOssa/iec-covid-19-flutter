import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/exams_controller.dart';
import 'package:iec_covid_19_flutter/widgets/app_select.dart';

final examsProvider = SimpleProvider((ref) => ExamsController());

class SelectExams extends StatelessWidget {
  final int? value;
  final void Function(int) onChanged;

  const SelectExams({Key? key, this.value, required this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(examsProvider);
        return AppSelect<int>(
          value: value,
          onChanged: (v) => onChanged(v!),
          items: ctrl.exams.map((value) => value.toMap()).toList(),
        );
      },
    );
  }
}
