import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/widgets/app_input.dart';
import 'package:iec_covid_19_flutter/widgets/app_input_date.dart';
import 'package:iec_covid_19_flutter/widgets/app_label.dart';
import 'package:iec_covid_19_flutter/widgets/button_danger.dart';
import 'package:iec_covid_19_flutter/widgets/form_group.dart';

class FormHospitalizationsItem extends StatelessWidget {
  final int index;
  final DateTime? date;
  final String? institution;
  final VoidCallback onDelete;
  final void Function(String)? onInstitutionChanged;
  final void Function(DateTime?)? onDateChanged;

  const FormHospitalizationsItem({
    Key? key,
    required this.index,
    required this.onDelete,
    this.date,
    this.institution,
    this.onInstitutionChanged,
    this.onDateChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.only(top: 16),
      child: size.width > 425
          ? FormHospitalizationsItemBodyDesktop(
              index: index,
              date: date,
              onDateChanged: onDateChanged,
              institution: institution,
              onInstitutionChanged: onInstitutionChanged,
              onDelete: onDelete,
            )
          : FormHospitalizationsItemBodyMobile(
              index: index,
              date: date,
              onDateChanged: onDateChanged,
              institution: institution,
              onInstitutionChanged: onInstitutionChanged,
              onDelete: onDelete,
            ),
    );
  }
}

class FormHospitalizationsItemBodyDesktop extends StatelessWidget {
  const FormHospitalizationsItemBodyDesktop({
    Key? key,
    required this.index,
    required this.date,
    required this.onDateChanged,
    required this.institution,
    required this.onInstitutionChanged,
    required this.onDelete,
  }) : super(key: key);

  final int index;
  final DateTime? date;
  final void Function(DateTime? p1)? onDateChanged;
  final String? institution;
  final void Function(String p1)? onInstitutionChanged;
  final VoidCallback onDelete;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
          flex: 2,
          child: FormGroup(
            label: AppLabel('Fecha de consulta $index'),
            input: AppInputDate(
              initialValue: date,
              onChanged: onDateChanged,
            ),
          ),
        ),
        SizedBox(width: 24),
        Flexible(
          flex: 2,
          child: FormGroup(
            label: AppLabel('Institución de salud'),
            input: AppInput(
              initialValue: institution,
              onChanged: onInstitutionChanged,
            ),
          ),
        ),
        SizedBox(width: 24),
        Flexible(
          child: ButtonDanger(
            child: Icon(Icons.delete),
            onPressed: onDelete,
          ),
        )
      ],
    );
  }
}

class FormHospitalizationsItemBodyMobile extends StatelessWidget {
  const FormHospitalizationsItemBodyMobile({
    Key? key,
    required this.index,
    required this.date,
    required this.onDateChanged,
    required this.institution,
    required this.onInstitutionChanged,
    required this.onDelete,
  }) : super(key: key);

  final int index;
  final DateTime? date;
  final void Function(DateTime? p1)? onDateChanged;
  final String? institution;
  final void Function(String p1)? onInstitutionChanged;
  final VoidCallback onDelete;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Hospitalización $index'),
        FormGroup(
          label: AppLabel('Fecha de consulta'),
          input: AppInputDate(
            initialValue: date,
            onChanged: onDateChanged,
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Institución de salud'),
          input: AppInput(
            initialValue: institution,
            onChanged: onInstitutionChanged,
          ),
        ),
        SizedBox(height: 20),
        ButtonDanger(
          child: Text('Quitar hospitalización $index'),
          onPressed: onDelete,
        )
      ],
    );
  }
}
