import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/widgets/button_secondary.dart';
import 'package:iec_covid_19_flutter/widgets/form_hospitalizations/form_hospitalizations_item.dart';

class FormHospitalizations extends StatelessWidget {
  final List<Map<String, dynamic>> hospitalizations;
  final VoidCallback onAddHospitalization;
  final void Function(dynamic, DateTime?) onDateChanged;
  final void Function(dynamic, String) onInstitutionChanged;
  final void Function(dynamic) onDelete;

  const FormHospitalizations({
    Key? key,
    required this.hospitalizations,
    required this.onAddHospitalization,
    required this.onDateChanged,
    required this.onInstitutionChanged,
    required this.onDelete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int counter = 0;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...hospitalizations.map<Widget>((Map<String, dynamic> hospitalization) {
          return FormHospitalizationsItem(
            index: ++counter,
            date: hospitalization['date'],
            institution: hospitalization['institution'],
            onDateChanged: (DateTime? date) {
              onDateChanged(hospitalization['id'], date);
            },
            onInstitutionChanged: (String institution) {
              onInstitutionChanged(hospitalization['id'], institution);
            },
            onDelete: () {
              onDelete(hospitalization['id']);
            },
          );
        }),
        Container(
          margin: EdgeInsets.only(top: 8),
          child: ButtonSecondary(
            child: Text('+ Añadir hospitalización'),
            onPressed: onAddHospitalization,
          ),
        ),
      ],
    );
  }
}
