import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/cities_controller.dart';
import 'package:iec_covid_19_flutter/widgets/app_select.dart';

final citiesProvider = SimpleProvider((ref) => CitiesController(ref.arguments));

class SelectCities extends StatelessWidget {
  final int value;
  final int regionId;
  final void Function(int) onChanged;

  const SelectCities({
    Key? key,
    required this.value,
    required this.regionId,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    citiesProvider.setArguments(regionId);
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(citiesProvider);
        return AppSelect<int>(
          value: ctrl.cities.any((city) => city.id == value) ? value : null,
          onChanged: (v) => onChanged(v!),
          items: ctrl.cities.map((value) => value.toMap()).toList(),
        );
      },
    );
  }
}
