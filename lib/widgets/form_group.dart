import 'package:flutter/material.dart';

class FormGroup extends StatelessWidget {
  final Widget label;
  final Widget input;

  const FormGroup({Key? key, required this.label, required this.input})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        label,
        Container(
          margin: EdgeInsets.only(top: 4),
          alignment: Alignment.topLeft,
          child: input,
        ),
      ],
    );
  }
}
