import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/create_medication_controller.dart';
import 'package:iec_covid_19_flutter/models/medication_model.dart';
import 'package:iec_covid_19_flutter/widgets/app_input.dart';
import 'package:iec_covid_19_flutter/widgets/button_secondary.dart';

final createMedicationProvider =
    SimpleProvider((ref) => CreateMedicationController());

class FormMedication extends StatelessWidget {
  final void Function(MedicationModel)? onCreated;

  const FormMedication({Key? key, this.onCreated}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(createMedicationProvider);
        return Row(
          children: [
            Flexible(
              child: AppInput(
                hintText: 'Nombre del nuevo medicamento',
                onChanged: (String value) {
                  ctrl.name = value;
                },
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              child: ButtonSecondary(
                child: Text('Agregar nuevo medicamento'),
                onPressed: () async {
                  if (ctrl.name == '') {
                    return null;
                  }
                  MedicationModel medication = await ctrl.store();
                  if (onCreated != null) {
                    onCreated!(medication);
                  }
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
