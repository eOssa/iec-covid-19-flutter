import 'package:flutter/material.dart';

class AppLabel extends StatelessWidget {
  final String text;

  const AppLabel(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 14,
        color: Colors.grey[700],
        fontWeight: FontWeight.w500,
      ),
    );
  }
}
