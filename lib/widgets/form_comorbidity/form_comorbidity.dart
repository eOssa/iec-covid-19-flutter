import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/create_comorbidity_controller.dart';
import 'package:iec_covid_19_flutter/models/comorbidity_model.dart';
import 'package:iec_covid_19_flutter/widgets/app_input.dart';
import 'package:iec_covid_19_flutter/widgets/button_secondary.dart';

final createComorbidityProvider =
    SimpleProvider((ref) => CreateComorbidityController());

class FormComorbidity extends StatelessWidget {
  final void Function(ComorbidityModel)? onCreated;

  const FormComorbidity({Key? key, this.onCreated}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(createComorbidityProvider);
        return Row(
          children: [
            Flexible(
              child: AppInput(
                hintText: 'Nombre del nuevo antecedente',
                onChanged: (String value) {
                  ctrl.name = value;
                },
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              child: ButtonSecondary(
                child: Text('Agregar nuevo antecedente'),
                onPressed: () async {
                  if (ctrl.name == '') {
                    return null;
                  }
                  ComorbidityModel comorbidity = await ctrl.store();
                  if (onCreated != null) {
                    onCreated!(comorbidity);
                  }
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
