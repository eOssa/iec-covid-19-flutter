import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/helpers/date.dart';

class AppInputDate extends StatelessWidget {
  final String format;
  final DateTime? initialValue;
  final String? Function(DateTime?)? validator;
  final void Function(DateTime?)? onChanged;
  final bool showResetIcon;

  AppInputDate({
    Key? key,
    this.format: 'dd/MM/yyyy',
    this.validator,
    this.onChanged,
    this.initialValue,
    this.showResetIcon: false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DateTimePicker(
      type: format.contains('HH:mm')
          ? DateTimePickerType.dateTime
          : DateTimePickerType.date,
      validator: (value) {
        if (validator != null) {
          if (value == 'null') {
            value = null;
          }
          return validator!(DateHelper.parse(value));
        }
      },
      onChanged: (value) {
        if (onChanged != null) {
          onChanged!(DateHelper.parse(value));
        }
      },
      dateMask: format,
      cancelText: 'CANCELAR',
      locale: const Locale('es', 'CO'),
      firstDate: DateTime(1900),
      initialDate: DateTime.now(),
      lastDate: DateTime(2100),
      initialValue: initialValue.toString(),
      decoration: InputDecoration(
        suffixIcon: showResetIcon && initialValue != null
            ? null
            : Icon(Icons.calendar_today),
        isDense: true,
        hintText: format,
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6),
          borderSide: BorderSide(color: Colors.indigo.shade500, width: 2.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6),
          borderSide: BorderSide(color: Colors.grey.shade300, width: 1.0),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6),
          borderSide:
              BorderSide(color: Theme.of(context).errorColor, width: 1.0),
        ),
      ),
    );
  }
}
