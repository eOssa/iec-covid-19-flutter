import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/create_iec_controller.dart';
import 'package:iec_covid_19_flutter/ui/views/create_iec_view.dart';
import 'package:iec_covid_19_flutter/widgets/app_input.dart';
import 'package:iec_covid_19_flutter/widgets/app_input_date.dart';
import 'package:iec_covid_19_flutter/widgets/app_label.dart';
import 'package:iec_covid_19_flutter/widgets/container_form.dart';
import 'package:iec_covid_19_flutter/widgets/form_group.dart';
import 'package:iec_covid_19_flutter/widgets/form_panel.dart';
import 'package:iec_covid_19_flutter/widgets/select_exams.dart';
import 'package:iec_covid_19_flutter/widgets/select_samples_types.dart';

class FormLaboratory extends StatelessWidget {
  final int id;

  FormLaboratory({required this.id});

  @override
  Widget build(BuildContext context) {
    return ContainerForm(
      panel: FormPanel(
        text: 'Datos de laboratorio',
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 2,
            ),
          ],
        ),
        child: Consumer(
          builder: (_, ref, __) {
            final size = MediaQuery.of(context).size;
            final ctrl = ref.watch(createIecProvider.find('iecs.$id'));
            return size.width > 425
                ? FormLaboratoryBodyDesktop(ctrl: ctrl)
                : FormLaboratoryBodyMobile(ctrl: ctrl);
          },
        ),
      ),
    );
  }
}

class FormLaboratoryBodyDesktop extends StatelessWidget {
  const FormLaboratoryBodyDesktop({
    Key? key,
    required this.ctrl,
  }) : super(key: key);

  final CreateIecController ctrl;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Flexible(
              flex: 3,
              child: FormGroup(
                label: AppLabel('Fecha de toma de primera muestra'),
                input: AppInputDate(
                  initialValue: ctrl.sampleDate,
                  onChanged: (DateTime? val) => ctrl.setSampleDate(val),
                  validator: (DateTime? value) {
                    if (!ctrl.effective || value != null) {
                      return null;
                    }
                    return 'Por favor ingresa una fecha de toma de muestra.';
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 3,
              child: FormGroup(
                label: AppLabel('Laboratorio'),
                input: AppInput(
                  initialValue: ctrl.sampleLaboratory,
                  onChanged: (String v) => ctrl.setSampleLaboratory(v),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 3,
              child: FormGroup(
                label: AppLabel('Tipo de muestra'),
                input: SelectSamplesTypes(
                  value: ctrl.sampleTypeId,
                  onChanged: (int value) => ctrl.setSampleTypeId(value),
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 3,
              child: FormGroup(
                label: AppLabel('Examen'),
                input: SelectExams(
                  value: ctrl.examId,
                  onChanged: (int value) => ctrl.setExamId(value),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class FormLaboratoryBodyMobile extends StatelessWidget {
  const FormLaboratoryBodyMobile({
    Key? key,
    required this.ctrl,
  }) : super(key: key);

  final CreateIecController ctrl;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        FormGroup(
          label: AppLabel('Fecha de toma de primera muestra'),
          input: AppInputDate(
            initialValue: ctrl.sampleDate,
            onChanged: (DateTime? val) => ctrl.setSampleDate(val),
            validator: (DateTime? value) {
              if (!ctrl.effective || value != null) {
                return null;
              }
              return 'Por favor ingresa una fecha de toma de muestra.';
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Laboratorio'),
          input: AppInput(
            initialValue: ctrl.sampleLaboratory,
            onChanged: (String v) => ctrl.setSampleLaboratory(v),
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Tipo de muestra'),
          input: SelectSamplesTypes(
            value: ctrl.sampleTypeId,
            onChanged: (int value) => ctrl.setSampleTypeId(value),
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Examen'),
          input: SelectExams(
            value: ctrl.examId,
            onChanged: (int value) => ctrl.setExamId(value),
          ),
        ),
      ],
    );
  }
}
