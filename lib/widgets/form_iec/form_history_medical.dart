import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/helpers/str.dart';
import 'package:iec_covid_19_flutter/ui/views/create_iec_view.dart';
import 'package:iec_covid_19_flutter/widgets/app_label.dart';
import 'package:iec_covid_19_flutter/widgets/container_form.dart';
import 'package:iec_covid_19_flutter/widgets/form_group.dart';
import 'package:iec_covid_19_flutter/widgets/form_hospitalizations/form_hospitalizations.dart';
import 'package:iec_covid_19_flutter/widgets/form_medication/form_medication.dart';
import 'package:iec_covid_19_flutter/widgets/form_panel.dart';
import 'package:iec_covid_19_flutter/widgets/form_symptom/form_symptom.dart';
import 'package:iec_covid_19_flutter/widgets/select_medications.dart';
import 'package:iec_covid_19_flutter/widgets/select_symptoms.dart';

class FormHistoryMedical extends StatelessWidget {
  final int id;

  FormHistoryMedical({required this.id});

  @override
  Widget build(BuildContext context) {
    return ContainerForm(
      panel: FormPanel(
        text: 'Antecedentes clínicos y de hospitalización',
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 2,
            ),
          ],
        ),
        child: Consumer(
          builder: (_, ref, __) {
            final ctrl = ref.watch(createIecProvider.find('iecs.$id'));
            return Column(
              children: <Widget>[
                FormGroup(
                  label: AppLabel('Hospitalizaciones'),
                  input: FormHospitalizations(
                    hospitalizations: ctrl.hospitalizations,
                    onAddHospitalization: () {
                      ctrl.addHospitalization({
                        'date': null,
                        'id': Str.getRandomString(5),
                        'institution': '',
                      });
                    },
                    onDateChanged: (id, DateTime? date) {
                      ctrl.updateHospitalizationDate(id, date);
                    },
                    onInstitutionChanged: (id, String institution) {
                      ctrl.updateHospitalizationInstitution(id, institution);
                    },
                    onDelete: (id) => ctrl.deleteHospitalization(id),
                  ),
                ),
                SizedBox(height: 20),
                FormGroup(
                  label: AppLabel('Medicamentos que se encuentra tomando'),
                  input: SelectMedications(
                    key: ValueKey(ctrl.medications.length),
                    value: ctrl.medications,
                    onConfirm: (List<int> value) => ctrl.setMedications(value),
                  ),
                ),
                SizedBox(height: 20),
                FormMedication(
                  key: ValueKey('medications${ctrl.medications.length}'),
                  onCreated: (medication) => ctrl.addMedication(medication.id),
                ),
                SizedBox(height: 20),
                FormGroup(
                  label: AppLabel('Signos y síntomas'),
                  input: SelectSymptoms(
                    key: ValueKey(ctrl.symptoms.length),
                    value: ctrl.symptoms,
                    onConfirm: (List<int> value) {
                      ctrl.setSymptoms(
                          value.map((int? item) => item!).toList());
                    },
                  ),
                ),
                SizedBox(height: 20),
                FormSymptom(
                  key: ValueKey('symptoms${ctrl.symptoms.length}'),
                  onCreated: (symptom) => ctrl.addSymptom(symptom.id),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
