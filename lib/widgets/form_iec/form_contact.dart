import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/create_iec_controller.dart';
import 'package:iec_covid_19_flutter/ui/views/create_iec_view.dart';
import 'package:iec_covid_19_flutter/widgets/app_input.dart';
import 'package:iec_covid_19_flutter/widgets/app_input_date.dart';
import 'package:iec_covid_19_flutter/widgets/app_label.dart';
import 'package:iec_covid_19_flutter/widgets/app_radio.dart';
import 'package:iec_covid_19_flutter/widgets/container_form.dart';
import 'package:iec_covid_19_flutter/widgets/form_group.dart';
import 'package:iec_covid_19_flutter/widgets/form_panel.dart';
import 'package:iec_covid_19_flutter/widgets/select_cities.dart';
import 'package:iec_covid_19_flutter/widgets/select_genders.dart';
import 'package:iec_covid_19_flutter/widgets/select_identifications_types.dart';
import 'package:iec_covid_19_flutter/widgets/select_regions.dart';
import 'package:iec_covid_19_flutter/widgets/select_zones.dart';

class FormContact extends StatelessWidget {
  final int id;

  FormContact({required this.id});

  @override
  Widget build(BuildContext context) {
    return ContainerForm(
      panel: FormPanel(
        text: 'Datos generales del paciente',
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 2,
            ),
          ],
        ),
        child: Consumer(
          builder: (_, ref, __) {
            final size = MediaQuery.of(context).size;
            final ctrl = ref.watch(createIecProvider.find('iecs.$id'));
            return size.width > 425
                ? FormContactBodyDesktop(ctrl: ctrl)
                : FormContactBodyMobile(ctrl: ctrl);
          },
        ),
      ),
    );
  }
}

class FormContactBodyDesktop extends StatelessWidget {
  const FormContactBodyDesktop({
    Key? key,
    required this.ctrl,
  }) : super(key: key);

  final CreateIecController ctrl;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: [
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Fecha de investigación'),
                input: AppInputDate(
                  initialValue: ctrl.date,
                  format: 'dd-MM-yyyy HH:mm',
                  onChanged: (DateTime? value) => ctrl.setDate(value),
                  validator: (DateTime? value) {
                    if (value != null) {
                      return null;
                    }
                    return 'Por favor ingresa una fecha.';
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 1,
              child: FormGroup(
                label: Text(''),
                input: AppRadio<bool>(
                  groupValue: ctrl.effective,
                  value: true,
                  title: Text('Efectivo'),
                  onChanged: (bool? value) => ctrl.setEffective(value),
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 3,
              child: FormGroup(
                label: Text(''),
                input: AppRadio<bool>(
                  groupValue: ctrl.effective,
                  value: false,
                  title: Text('No Efectivo'),
                  onChanged: (bool? value) => ctrl.setEffective(value),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 3,
              child: FormGroup(
                label: AppLabel('Primer nombre'),
                input: AppInput(
                  initialValue: ctrl.firstName,
                  // autofocus: true,
                  onChanged: ctrl.setFirstName,
                  validator: (String? value) {
                    if (value != null && value.isNotEmpty) {
                      return null;
                    }
                    return 'Por favor ingresa el primer nombre del paciente.';
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 3,
              child: FormGroup(
                label: AppLabel('Segundo nombre'),
                input: AppInput(
                  initialValue: ctrl.middleName,
                  onChanged: ctrl.setMiddleName,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 4,
              child: FormGroup(
                label: AppLabel('Primer apellido'),
                input: AppInput(
                  initialValue: ctrl.lastName,
                  onChanged: ctrl.setLastName,
                  validator: (String? value) {
                    if (value != null && value.isNotEmpty) {
                      return null;
                    }
                    return 'Por favor ingresa el primer apellido del paciente.';
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 4,
              child: FormGroup(
                label: AppLabel('Segundo apellido'),
                input: AppInput(
                  initialValue: ctrl.secondLastName,
                  onChanged: (String? val) => ctrl.setSecondLastName(val),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('EPS'),
                input: AppInput(
                  initialValue: ctrl.eps,
                  onChanged: ctrl.setEps,
                  validator: (String? v) {
                    if (!ctrl.effective || v != null && v.isNotEmpty) {
                      return null;
                    }
                    return 'Por favor ingresa la EPS del paciente.';
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Tipo de documento'),
                input: SelectIdentificationsTypes(
                  value: ctrl.identificationTypeId,
                  onChanged: (int v) => ctrl.setIdentificationTypeId(v),
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Documento de identidad'),
                input: AppInput(
                  initialValue: ctrl.identification,
                  onChanged: (String? val) => ctrl.setIdentification(val),
                  validator: (String? value) {
                    if (value != null && value.isNotEmpty) {
                      return null;
                    }
                    return 'Por favor ingresa el documento de identidad del paciente.';
                  },
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 4,
              child: FormGroup(
                label: AppLabel('Fecha nacimiento'),
                input: AppInputDate(
                  initialValue: ctrl.birthdate,
                  onChanged: (DateTime? val) => ctrl.setBirthdate(val),
                  validator: (DateTime? value) {
                    if (!ctrl.effective || value != null) {
                      return null;
                    }
                    return 'Por favor ingresa una fecha de nacimiento.';
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppLabel('Edad'),
                  Container(
                    padding: EdgeInsets.symmetric(
                      vertical: 11,
                      horizontal: 6,
                    ),
                    child: Text(ctrl.age.toString()),
                  ),
                ],
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Sexo'),
                input: SelectGenders(
                  value: ctrl.gender,
                  onChanged: (int value) => ctrl.setGender(value),
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 4,
              child: FormGroup(
                label: AppLabel('Nacionalidad'),
                input: AppInput(
                  key: ValueKey(ctrl.effective),
                  initialValue: ctrl.nationality,
                  onChanged: (String? val) => ctrl.setNationality(val),
                  validator: (String? v) {
                    if (!ctrl.effective || v != null && v.isNotEmpty) {
                      return null;
                    }
                    return 'Por favor ingresa la nacionalidad del paciente.';
                  },
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Dirección'),
          input: AppInput(
            initialValue: ctrl.address,
            onChanged: ctrl.setAddress,
            validator: (String? value) {
              if (!ctrl.effective || value != null && value.isNotEmpty) {
                return null;
              }
              return 'Por favor ingresa la dirección del paciente.';
            },
          ),
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Departamento'),
                input: SelectRegions(
                  value: ctrl.regionId!,
                  onChanged: (int value) {
                    ctrl.setCityId(0);
                    ctrl.setRegionId(value);
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Municipio'),
                input: SelectCities(
                  key: ValueKey(ctrl.regionId),
                  value: ctrl.cityId!,
                  regionId: ctrl.regionId!,
                  onChanged: (int value) => ctrl.setCityId(value),
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Barrio'),
                input: AppInput(
                  initialValue: ctrl.neighborhood,
                  onChanged: (String? v) => ctrl.setNeighborhood(v),
                  validator: (String? v) {
                    if (!ctrl.effective || v != null && v.isNotEmpty) {
                      return null;
                    }
                    return 'Por favor ingresa el barrio del paciente.';
                  },
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Zona de residencia'),
                input: SelectZones(
                  value: ctrl.zone!,
                  onChanged: (int value) => ctrl.setZone(value),
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Tel. de contacto 1'),
                input: AppInput(
                  initialValue: ctrl.primaryPhone,
                  onChanged: (String? v) => ctrl.setPrimaryPhone(v),
                  validator: (String? value) {
                    if (value != null && value.isNotEmpty) {
                      return null;
                    }
                    return 'Por favor ingresa el primer teléfono del paciente.';
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Tel. de contacto 2'),
                input: AppInput(
                  initialValue: ctrl.secondaryPhone,
                  onChanged: (String? v) => ctrl.setSecondaryPhone(v),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Ocupación'),
          input: AppInput(
            initialValue: ctrl.occupation,
            onChanged: ctrl.setOccupation,
          ),
        ),
      ],
    );
  }
}

class FormContactBodyMobile extends StatelessWidget {
  const FormContactBodyMobile({
    Key? key,
    required this.ctrl,
  }) : super(key: key);

  final CreateIecController ctrl;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        FormGroup(
          label: AppLabel('Fecha de investigación'),
          input: AppInputDate(
            initialValue: ctrl.date,
            format: 'dd-MM-yyyy HH:mm',
            onChanged: (DateTime? value) => ctrl.setDate(value),
            validator: (DateTime? value) {
              if (value != null) {
                return null;
              }
              return 'Por favor ingresa una fecha.';
            },
          ),
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              child: AppRadio<bool>(
                groupValue: ctrl.effective,
                value: true,
                title: Text('Efectivo'),
                onChanged: ctrl.setEffective,
              ),
            ),
            Flexible(
              child: AppRadio<bool>(
                groupValue: ctrl.effective,
                value: false,
                title: Text('No Efectivo'),
                onChanged: ctrl.setEffective,
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Primer nombre'),
          input: AppInput(
            initialValue: ctrl.firstName,
            // autofocus: true,
            onChanged: ctrl.setFirstName,
            validator: (String? value) {
              if (value != null && value.isNotEmpty) {
                return null;
              }
              return 'Por favor ingresa el primer nombre del paciente.';
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Segundo nombre'),
          input: AppInput(
            initialValue: ctrl.middleName,
            onChanged: ctrl.setMiddleName,
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Primer apellido'),
          input: AppInput(
            initialValue: ctrl.lastName,
            onChanged: ctrl.setLastName,
            validator: (String? value) {
              if (value != null && value.isNotEmpty) {
                return null;
              }
              return 'Por favor ingresa el primer apellido del paciente.';
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Segundo apellido'),
          input: AppInput(
            initialValue: ctrl.secondLastName,
            onChanged: (String? val) => ctrl.setSecondLastName(val),
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('EPS'),
          input: AppInput(
            initialValue: ctrl.eps,
            onChanged: ctrl.setEps,
            validator: (String? v) {
              if (!ctrl.effective || v != null && v.isNotEmpty) {
                return null;
              }
              return 'Por favor ingresa la EPS del paciente.';
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Tipo de documento'),
          input: SelectIdentificationsTypes(
            value: ctrl.identificationTypeId,
            onChanged: (int v) => ctrl.setIdentificationTypeId(v),
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Documento de identidad'),
          input: AppInput(
            initialValue: ctrl.identification,
            onChanged: (String? val) => ctrl.setIdentification(val),
            validator: (String? value) {
              if (value != null && value.isNotEmpty) {
                return null;
              }
              return 'Por favor ingresa el documento de identidad del paciente.';
            },
          ),
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 9,
              child: FormGroup(
                label: AppLabel('Fecha nacimiento'),
                input: AppInputDate(
                  initialValue: ctrl.birthdate,
                  onChanged: (DateTime? val) => ctrl.setBirthdate(val),
                  validator: (DateTime? value) {
                    if (!ctrl.effective || value != null) {
                      return null;
                    }
                    return 'Por favor ingresa una fecha de nacimiento.';
                  },
                ),
              ),
            ),
            Spacer(),
            Flexible(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppLabel('Edad'),
                  Container(
                    padding: EdgeInsets.symmetric(
                      vertical: 11,
                      horizontal: 6,
                    ),
                    child: Text(ctrl.age.toString()),
                  ),
                ],
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 3,
              child: FormGroup(
                label: AppLabel('Sexo'),
                input: SelectGenders(
                  value: ctrl.gender,
                  onChanged: (int value) => ctrl.setGender(value),
                ),
              ),
            ),
            Spacer(),
            Flexible(
              flex: 8,
              child: FormGroup(
                label: AppLabel('Nacionalidad'),
                input: AppInput(
                  key: ValueKey(ctrl.effective),
                  initialValue: ctrl.nationality,
                  onChanged: (String? val) => ctrl.setNationality(val),
                  validator: (String? v) {
                    if (!ctrl.effective || v != null && v.isNotEmpty) {
                      return null;
                    }
                    return 'Por favor ingresa la nacionalidad del paciente.';
                  },
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Dirección'),
          input: AppInput(
            initialValue: ctrl.address,
            onChanged: ctrl.setAddress,
            validator: (String? value) {
              if (!ctrl.effective || value != null && value.isNotEmpty) {
                return null;
              }
              return 'Por favor ingresa la dirección del paciente.';
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Departamento'),
          input: SelectRegions(
            value: ctrl.regionId!,
            onChanged: (int value) {
              ctrl.setCityId(0);
              ctrl.setRegionId(value);
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Municipio'),
          input: SelectCities(
            key: ValueKey(ctrl.regionId),
            value: ctrl.cityId!,
            regionId: ctrl.regionId!,
            onChanged: (int value) => ctrl.setCityId(value),
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Barrio'),
          input: AppInput(
            initialValue: ctrl.neighborhood,
            onChanged: (String? v) => ctrl.setNeighborhood(v),
            validator: (String? v) {
              if (!ctrl.effective || v != null && v.isNotEmpty) {
                return null;
              }
              return 'Por favor ingresa el barrio del paciente.';
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Zona de residencia'),
          input: SelectZones(
            value: ctrl.zone!,
            onChanged: (int value) => ctrl.setZone(value),
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Tel. de contacto 1'),
          input: AppInput(
            initialValue: ctrl.primaryPhone,
            onChanged: (String? v) => ctrl.setPrimaryPhone(v),
            validator: (String? value) {
              if (value != null && value.isNotEmpty) {
                return null;
              }
              return 'Por favor ingresa el primer teléfono del paciente.';
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Tel. de contacto 2'),
          input: AppInput(
            initialValue: ctrl.secondaryPhone,
            onChanged: (String? v) => ctrl.setSecondaryPhone(v),
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Ocupación'),
          input: AppInput(
            initialValue: ctrl.occupation,
            onChanged: ctrl.setOccupation,
          ),
        ),
      ],
    );
  }
}
