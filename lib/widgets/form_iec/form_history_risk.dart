import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/create_iec_controller.dart';
import 'package:iec_covid_19_flutter/helpers/str.dart';
import 'package:iec_covid_19_flutter/ui/views/create_iec_view.dart';
import 'package:iec_covid_19_flutter/widgets/app_input.dart';
import 'package:iec_covid_19_flutter/widgets/app_input_date.dart';
import 'package:iec_covid_19_flutter/widgets/app_label.dart';
import 'package:iec_covid_19_flutter/widgets/app_radio.dart';
import 'package:iec_covid_19_flutter/widgets/container_form.dart';
import 'package:iec_covid_19_flutter/widgets/form_displacements/form_displacements.dart';
import 'package:iec_covid_19_flutter/widgets/form_group.dart';
import 'package:iec_covid_19_flutter/widgets/form_panel.dart';

class FormHistoryRisk extends StatelessWidget {
  final int id;

  FormHistoryRisk({required this.id});

  @override
  Widget build(BuildContext context) {
    return ContainerForm(
      panel: FormPanel(
        text: 'Antecedentes de riesgo y exposición',
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 2,
            ),
          ],
        ),
        child: Consumer(
          builder: (_, ref, __) {
            final size = MediaQuery.of(context).size;
            final ctrl = ref.watch(createIecProvider.find('iecs.$id'));
            return size.width > 425
                ? FormHistoryRiskBodyDesktop(ctrl: ctrl)
                : FormHistoryRiskBodyMobile(ctrl: ctrl);
          },
        ),
      ),
    );
  }
}

class FormHistoryRiskBodyDesktop extends StatelessWidget {
  const FormHistoryRiskBodyDesktop({
    Key? key,
    required this.ctrl,
  }) : super(key: key);

  final CreateIecController ctrl;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Fecha de inicio de síntomas'),
                input: AppInputDate(
                  initialValue: ctrl.symptomDate,
                  onChanged: (DateTime? val) => ctrl.setSymptomDate(val),
                  validator: (DateTime? value) {
                    if (!ctrl.effective || value != null) {
                      return null;
                    }
                    return 'Por favor ingresa una fecha de inicio de síntomas.';
                  },
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Container(),
            ),
          ],
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Desplazamientos en los últimos días'),
          input: FormDisplacements(
            displacements: ctrl.displacements,
            onAddDisplacement: () {
              ctrl.addDisplacement({
                'city': null,
                'country': 'Colombia',
                'endDate': null,
                'id': Str.getRandomString(5),
                'startDate': null,
              });
            },
            onCityChanged: (id, String city) {
              ctrl.updateDisplacementCity(id, city);
            },
            onCountryChanged: (id, String country) {
              ctrl.updateDisplacementCountry(id, country);
            },
            onEndDateChanged: (id, DateTime? endDate) {
              ctrl.updateDisplacementEndDate(id, endDate);
            },
            onStartDateChanged: (id, DateTime? startDate) {
              ctrl.updateDisplacementStartDate(id, startDate);
            },
            onDelete: (id) => ctrl.deleteDisplacement(id),
          ),
        ),
        SizedBox(height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Flexible(
              flex: 4,
              child: FormGroup(
                label: Text(
                  '¿Tuvo contacto con un caso positivo o probable de Covid-19?',
                ),
                input: Column(
                  children: [
                    AppRadio<bool>(
                      groupValue: ctrl.withPositiveContact,
                      value: true,
                      title: Text('Sí'),
                      onChanged: (v) => ctrl.setWithPositiveContact(v),
                    ),
                    AppRadio<bool>(
                      groupValue: ctrl.withPositiveContact,
                      value: false,
                      title: Text('No'),
                      onChanged: (v) => ctrl.setWithPositiveContact(v),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Lugar de contacto'),
                input: AppInput(
                  initialValue: ctrl.positiveContactPlace,
                  onChanged: (val) => ctrl.setPositiveContactPlace(val),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class FormHistoryRiskBodyMobile extends StatelessWidget {
  const FormHistoryRiskBodyMobile({
    Key? key,
    required this.ctrl,
  }) : super(key: key);

  final CreateIecController ctrl;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        FormGroup(
          label: AppLabel('Fecha de inicio de síntomas'),
          input: AppInputDate(
            initialValue: ctrl.symptomDate,
            onChanged: (DateTime? val) => ctrl.setSymptomDate(val),
            validator: (DateTime? value) {
              if (!ctrl.effective || value != null) {
                return null;
              }
              return 'Por favor ingresa una fecha de inicio de síntomas.';
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Desplazamientos en los últimos días'),
          input: FormDisplacements(
            displacements: ctrl.displacements,
            onAddDisplacement: () {
              ctrl.addDisplacement({
                'city': null,
                'country': 'Colombia',
                'endDate': null,
                'id': Str.getRandomString(5),
                'startDate': null,
              });
            },
            onCityChanged: (id, String city) {
              ctrl.updateDisplacementCity(id, city);
            },
            onCountryChanged: (id, String country) {
              ctrl.updateDisplacementCountry(id, country);
            },
            onEndDateChanged: (id, DateTime? endDate) {
              ctrl.updateDisplacementEndDate(id, endDate);
            },
            onStartDateChanged: (id, DateTime? startDate) {
              ctrl.updateDisplacementStartDate(id, startDate);
            },
            onDelete: (id) => ctrl.deleteDisplacement(id),
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: Text(
            '¿Tuvo contacto con un caso positivo o probable de Covid-19?',
          ),
          input: Column(
            children: [
              AppRadio<bool>(
                groupValue: ctrl.withPositiveContact,
                value: true,
                title: Text('Sí'),
                onChanged: (v) => ctrl.setWithPositiveContact(v),
              ),
              AppRadio<bool>(
                groupValue: ctrl.withPositiveContact,
                value: false,
                title: Text('No'),
                onChanged: (v) => ctrl.setWithPositiveContact(v),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Lugar de contacto'),
          input: AppInput(
            initialValue: ctrl.positiveContactPlace,
            onChanged: (val) => ctrl.setPositiveContactPlace(val),
          ),
        ),
      ],
    );
  }
}
