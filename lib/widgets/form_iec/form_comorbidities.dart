import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/ui/views/create_iec_view.dart';
import 'package:iec_covid_19_flutter/widgets/app_input_number.dart';
import 'package:iec_covid_19_flutter/widgets/app_label.dart';
import 'package:iec_covid_19_flutter/widgets/container_form.dart';
import 'package:iec_covid_19_flutter/widgets/form_comorbidity/form_comorbidity.dart';
import 'package:iec_covid_19_flutter/widgets/form_group.dart';
import 'package:iec_covid_19_flutter/widgets/form_panel.dart';
import 'package:iec_covid_19_flutter/widgets/select_comorbidities.dart';

class FormComorbidities extends StatelessWidget {
  final int id;

  FormComorbidities({required this.id});

  @override
  Widget build(BuildContext context) {
    return ContainerForm(
      panel: FormPanel(
        text: 'Comorbilidades / Factores de riesgo',
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 2,
            ),
          ],
        ),
        child: Consumer(
          builder: (_, ref, __) {
            final size = MediaQuery.of(context).size;
            final ctrl = ref.watch(createIecProvider.find('iecs.$id'));
            return Column(
              children: [
                FormGroup(
                  label: AppLabel('Antecedentes'),
                  input: SelectComorbidities(
                    key: ValueKey(ctrl.comorbidities.length),
                    value: ctrl.comorbidities,
                    onConfirm: (List<int> comorbidities) {
                      ctrl.setComorbidities(comorbidities);
                    },
                  ),
                ),
                SizedBox(height: 20),
                FormComorbidity(
                  key: ValueKey('comorbidities${ctrl.comorbidities.length}'),
                  onCreated: (comorbidity) {
                    ctrl.addComorbidity(comorbidity.id);
                  },
                ),
                SizedBox(height: 20),
                Row(
                  children: [
                    Flexible(
                      child: FormGroup(
                        label: AppLabel('Semanas de embarazo'),
                        input: AppInputNumber(
                          initialValue: ctrl.pregnancyWeeks,
                          enabled: ctrl.comorbidities.contains(11),
                          onChanged: (int value) =>
                              ctrl.setPregnancyWeeks(value),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(),
                      flex: size.width > 425 ? 5 : 1,
                    ),
                  ],
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
