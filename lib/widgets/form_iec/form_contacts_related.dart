import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/widgets/container_form.dart';
import 'package:iec_covid_19_flutter/widgets/form_contacts_iec/form_contacts_iec.dart';
import 'package:iec_covid_19_flutter/widgets/form_panel.dart';

class FormContactsRelated extends StatelessWidget {
  final int id;

  FormContactsRelated({required this.id});

  @override
  Widget build(BuildContext context) {
    return ContainerForm(
      panel: FormPanel(
        text: 'Contactos estrechos',
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 2,
            ),
          ],
        ),
        child: FormContactsIec(id: id),
      ),
    );
  }
}
