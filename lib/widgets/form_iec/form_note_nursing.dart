import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/ui/views/create_iec_view.dart';
import 'package:iec_covid_19_flutter/widgets/app_input.dart';
import 'package:iec_covid_19_flutter/widgets/container_form.dart';
import 'package:iec_covid_19_flutter/widgets/form_panel.dart';

class FormNoteNursing extends StatelessWidget {
  final int id;

  FormNoteNursing({required this.id});

  @override
  Widget build(BuildContext context) {
    return ContainerForm(
      panel: FormPanel(
        text: 'Nota de enfermería',
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 2,
            ),
          ],
        ),
        child: Consumer(
          builder: (_, ref, __) {
            final ctrl = ref.watch(createIecProvider.find('iecs.$id'));
            return AppInput(
              key: ValueKey(ctrl.effective),
              controller: ctrl.nursingNoteController,
              maxLines: 7,
              onChanged: (String value) => ctrl.nursingNoteOnChange(),
            );
          },
        ),
      ),
    );
  }
}
