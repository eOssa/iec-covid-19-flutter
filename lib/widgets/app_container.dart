import 'package:flutter/material.dart';

class AppContainer extends StatelessWidget {
  final Widget? child;
  final Decoration? decoration;

  const AppContainer({Key? key, this.child, this.decoration}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      alignment: Alignment.topCenter,
      padding: EdgeInsets.only(top: 24),
      decoration: decoration,
      child: Container(
        width: 1280,
        padding: size.width > 425
            ? EdgeInsets.symmetric(horizontal: 32, vertical: 24)
            : EdgeInsets.zero,
        child: child,
      ),
    );
  }
}
