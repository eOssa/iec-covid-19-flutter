import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/samples_types_controller.dart';
import 'package:iec_covid_19_flutter/widgets/app_select.dart';

final samplesTypesProvider = SimpleProvider((ref) => SamplesTypesController());

class SelectSamplesTypes extends StatelessWidget {
  final int? value;
  final void Function(int)? onChanged;

  const SelectSamplesTypes({Key? key, this.value, this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(samplesTypesProvider);
        return AppSelect<int>(
          value: value,
          onChanged: (v) => onChanged!(v!),
          items: ctrl.samplesTypes.map((value) => value.toMap()).toList(),
        );
      },
    );
  }
}
