import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/widgets/app_button.dart';

class ButtonPrimary extends StatelessWidget {
  final Widget child;
  final VoidCallback onPressed;

  const ButtonPrimary({
    Key? key,
    required this.onPressed,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppButton(
      child: child,
      onPressed: onPressed,
      backgroundColor: Color(0xFF4F46E5),
    );
  }
}
