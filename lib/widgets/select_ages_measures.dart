import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/widgets/app_select.dart';

class SelectAgesMeasures extends StatelessWidget {
  final String? value;
  final void Function(String)? onChanged;

  const SelectAgesMeasures({Key? key, this.value, this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppSelect<String>(
      value: value,
      onChanged: (v) => onChanged!(v!),
      items: [
        {'id': 'year', 'name': 'Años'},
        {'id': 'month', 'name': 'Meses'},
        {'id': 'day', 'name': 'Días'},
      ],
    );
  }
}
