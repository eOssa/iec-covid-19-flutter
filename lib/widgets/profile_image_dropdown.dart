import 'package:flutter/material.dart';
import 'profile_image.dart';

class ProfileImageDropdown extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      padding: const EdgeInsets.all(16.0),
      child: ProfileImage(),
    );
    // return PopupMenuButton(
    //   icon: ProfileImage(),
    //   offset: Offset(0, 45),
    //   padding: EdgeInsets.all(5),
    //   itemBuilder: (context) => [
    //     PopupMenuItem(value: 1, child: Text('Your Profile')),
    //     PopupMenuItem(value: 2, child: Text('Settings')),
    //     PopupMenuItem(value: 3, child: Text('Logout')),
    //   ],
    // );
  }
}
