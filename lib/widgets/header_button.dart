import 'package:flutter/material.dart';

class HeaderButton extends StatelessWidget {
  final Widget? icon;
  final Widget child;
  final VoidCallback onPressed;
  final Color color;
  final bool disabled;

  const HeaderButton({
    Key? key,
    required this.child,
    required this.onPressed,
    this.icon,
    this.color: const Color(0xFF059669),
    this.disabled: false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return ElevatedButton.icon(
      icon: icon ?? Container(),
      label: child,
      onPressed: disabled ? null : onPressed,
      style: TextButton.styleFrom(
        primary: Colors.white,
        backgroundColor: color,
        padding: size.width > 425
            ? EdgeInsets.symmetric(horizontal: 36, vertical: 30)
            : EdgeInsets.symmetric(horizontal: 12, vertical: 16),
      ),
    );
  }
}
