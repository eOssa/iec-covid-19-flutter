import 'package:flutter/material.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

class AppMultiselect<V> extends StatelessWidget {
  final List<V>? value;
  final List<Map<V, dynamic>> items;
  final String idLabel;
  final String textLabel;
  final void Function(List<V>) onConfirm;
  final Text? buttonText;

  const AppMultiselect({
    Key? key,
    this.value,
    this.idLabel: 'id',
    this.textLabel: 'name',
    this.buttonText,
    required this.items,
    required this.onConfirm,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiSelectDialogField<V>(
      initialValue: value,
      buttonText: buttonText ?? Text('Selecciona'),
      buttonIcon: Icon(Icons.expand_more),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        border: Border.all(color: Colors.grey.shade300, width: 1.0),
      ),
      onConfirm: onConfirm,
      title: Text('Selecciona'),
      cancelText: Text(
        'CANCELAR',
        style: TextStyle(color: Color(0xFF4F46E5).withOpacity(1)),
      ),
      searchable: true,
      selectedColor: Color(0xFF4F46E5),
      items: items
          .map<MultiSelectItem<V>>(
            (item) => MultiSelectItem<V>(
              item[idLabel] as V,
              item[textLabel].toString(),
            ),
          )
          .toList(),
    );
  }
}
