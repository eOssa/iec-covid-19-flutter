import 'package:flutter/material.dart';

class AppCheckbox extends StatelessWidget {
  final bool value;
  final Widget? title;
  final void Function(bool?)? onChanged;

  const AppCheckbox({
    Key? key,
    required this.value,
    this.onChanged,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      title: title,
      value: value,
      onChanged: onChanged,
      controlAffinity: ListTileControlAffinity.leading,
      contentPadding: EdgeInsets.zero,
      dense: true,
    );
  }
}
