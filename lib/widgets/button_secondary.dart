import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/widgets/app_button.dart';

class ButtonSecondary extends StatelessWidget {
  final Widget child;
  final VoidCallback onPressed;

  const ButtonSecondary({
    Key? key,
    required this.onPressed,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppButton(
      child: child,
      onPressed: onPressed,
      backgroundColor: Colors.white,
      side: BorderSide(width: 1, color: Color(0xFF4F46E5)),
      textColor: Color(0xFF4F46E5),
    );
  }
}
