import 'package:flutter/material.dart';

class AppSelect<V> extends StatelessWidget {
  final V? value;
  final String idLabel;
  final String textLabel;
  final void Function(V?)? onChanged;
  final List<Map<String, dynamic>> items;
  final bool isExpanded;

  const AppSelect({
    Key? key,
    this.value,
    this.idLabel: 'id',
    this.textLabel: 'name',
    this.onChanged,
    this.isExpanded: true,
    required this.items,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        border: Border.all(color: Colors.grey.shade300, width: 1.0),
      ),
      child: DropdownButton<V>(
        value: value,
        icon: const Icon(Icons.expand_more),
        iconSize: 24,
        elevation: 16,
        underline: Container(),
        isDense: true,
        isExpanded: isExpanded,
        onChanged: onChanged,
        items: items.map((item) {
          return DropdownMenuItem<V>(
            value: item[idLabel],
            child: Text(item[textLabel]),
          );
        }).toList(),
      ),
    );
  }
}
