import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  final String text;
  final List<Widget> actions;

  const Header({Key? key, this.text: 'Iecs Covid-19', this.actions: const []})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.symmetric(vertical: 19.0, horizontal: 24.0),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            blurRadius: 2,
          ),
        ],
      ),
      child: size.width > 425
          ? HeaderBodyDesktop(text: text, actions: actions)
          : HeaderBodyMobile(text: text, actions: actions),
    );
  }
}

class HeaderBodyDesktop extends StatelessWidget {
  const HeaderBodyDesktop({
    Key? key,
    required this.text,
    required this.actions,
  }) : super(key: key);

  final String text;
  final List<Widget> actions;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Text(
            text,
            style: Theme.of(context).textTheme.headline3,
          ),
        ),
        ...actions.map((action) {
          return Container(
            margin: EdgeInsets.only(left: 16),
            child: action,
          );
        }),
      ],
    );
  }
}

class HeaderBodyMobile extends StatelessWidget {
  const HeaderBodyMobile({
    Key? key,
    required this.text,
    required this.actions,
  }) : super(key: key);

  final String text;
  final List<Widget> actions;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          text,
          style: Theme.of(context).textTheme.headline6,
        ),
        SizedBox(height: 10.0),
        ...actions.map((action) {
          return Container(
            margin: EdgeInsets.only(left: 16),
            child: action,
          );
        }).expand((element) => [element, SizedBox(height: 5.0)]),
      ],
    );
  }
}
