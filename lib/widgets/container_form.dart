import 'package:flutter/material.dart';

class ContainerForm extends StatelessWidget {
  final Widget child;
  final Widget panel;

  ContainerForm({required this.child, required this.panel});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.only(top: 20, left: 16, right: 16),
      decoration: BoxDecoration(color: Colors.grey.shade50),
      child: size.width > 425
          ? ContainerFormBodyDesktop(panel: panel, child: child)
          : ContainerFormBodyMobile(panel: panel, child: child),
    );
  }
}

class ContainerFormBodyDesktop extends StatelessWidget {
  const ContainerFormBodyDesktop({
    Key? key,
    required this.panel,
    required this.child,
  }) : super(key: key);

  final Widget panel;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.only(right: 16),
            child: panel,
          ),
        ),
        Expanded(
          child: child,
          flex: 2,
        ),
      ],
    );
  }
}

class ContainerFormBodyMobile extends StatelessWidget {
  const ContainerFormBodyMobile({
    Key? key,
    required this.panel,
    required this.child,
  }) : super(key: key);

  final Widget panel;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 16, bottom: 8),
          child: panel,
        ),
        child,
      ],
    );
  }
}
