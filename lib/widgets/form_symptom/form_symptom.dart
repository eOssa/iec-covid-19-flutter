import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/create_symptom_controller.dart';
import 'package:iec_covid_19_flutter/models/symptom_model.dart';
import 'package:iec_covid_19_flutter/widgets/app_input.dart';
import 'package:iec_covid_19_flutter/widgets/button_secondary.dart';

final createSymptomProvider =
    SimpleProvider((ref) => CreateSymptomController());

class FormSymptom extends StatelessWidget {
  final void Function(SymptomModel)? onCreated;

  const FormSymptom({Key? key, this.onCreated}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(createSymptomProvider);
        return Row(
          children: [
            Flexible(
              child: AppInput(
                hintText: 'Nombre del nuevo síntoma',
                onChanged: (String value) {
                  ctrl.name = value;
                },
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              child: ButtonSecondary(
                child: Text('Agregar nuevo síntoma'),
                onPressed: () async {
                  if (ctrl.name == '') {
                    return null;
                  }
                  SymptomModel symptom = await ctrl.store();
                  if (onCreated != null) {
                    onCreated!(symptom);
                  }
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
