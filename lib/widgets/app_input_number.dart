import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iec_covid_19_flutter/widgets/app_input.dart';

class AppInputNumber extends StatelessWidget {
  final int? initialValue;
  final String? Function(int?)? validator;
  final void Function(int)? onChanged;
  final bool autofocus;
  final bool enabled;

  const AppInputNumber({
    Key? key,
    this.initialValue,
    this.validator,
    this.onChanged,
    this.autofocus: false,
    this.enabled: true,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppInput(
      initialValue: initialValue.toString(),
      validator: (String? value) {
        if (validator != null) {
          validator!(value == null ? null : int.parse(value));
        }
      },
      onChanged: (String value) {
        if (onChanged != null) {
          onChanged!(int.parse(value != '' ? value : '0'));
        }
      },
      autofocus: autofocus,
      keyboardType: TextInputType.number,
      enabled: enabled,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
    );
  }
}
