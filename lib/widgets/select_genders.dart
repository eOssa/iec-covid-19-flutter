import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/genders_controller.dart';
import 'package:iec_covid_19_flutter/widgets/app_select.dart';

final gendersProvider = SimpleProvider((ref) => GendersController());

class SelectGenders extends StatelessWidget {
  final int value;
  final void Function(int) onChanged;

  const SelectGenders({Key? key, required this.value, required this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(gendersProvider);
        return AppSelect<int>(
          value: value,
          onChanged: (v) => onChanged(v!),
          items: ctrl.genders.map((value) => value.toMap()).toList(),
        );
      },
    );
  }
}
