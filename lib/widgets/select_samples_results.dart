import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/widgets/app_select.dart';

class SelectSamplesResults extends StatelessWidget {
  final String? value;
  final void Function(String?)? onChanged;

  const SelectSamplesResults({Key? key, this.value, this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppSelect<String>(
      value: value,
      onChanged: onChanged,
      items: [
        {'id': '', 'name': ''},
        {'id': 'Positivo', 'name': 'Positivo'},
        {'id': 'Negativo', 'name': 'Negativo'},
        {'id': 'Pendiente', 'name': 'Pendiente'},
      ],
    );
  }
}
