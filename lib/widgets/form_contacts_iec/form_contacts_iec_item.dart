import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/models/contact_model.dart';
import 'package:iec_covid_19_flutter/widgets/app_input.dart';
import 'package:iec_covid_19_flutter/widgets/app_input_date.dart';
import 'package:iec_covid_19_flutter/widgets/app_input_number.dart';
import 'package:iec_covid_19_flutter/widgets/app_label.dart';
import 'package:iec_covid_19_flutter/widgets/form_comorbidity/form_comorbidity.dart';
import 'package:iec_covid_19_flutter/widgets/form_group.dart';
import 'package:iec_covid_19_flutter/widgets/form_symptom/form_symptom.dart';
import 'package:iec_covid_19_flutter/widgets/select_ages_measures.dart';
import 'package:iec_covid_19_flutter/widgets/select_comorbidities.dart';
import 'package:iec_covid_19_flutter/widgets/select_genders.dart';
import 'package:iec_covid_19_flutter/widgets/select_identifications_types.dart';
import 'package:iec_covid_19_flutter/widgets/select_samples_results.dart';
import 'package:iec_covid_19_flutter/widgets/select_symptoms.dart';

class FormContactsIecItem extends StatelessWidget {
  final int index;
  final ContactModel contact;
  final void Function(ContactModel) onContactChange;
  final VoidCallback onDelete;

  const FormContactsIecItem(
    this.contact, {
    Key? key,
    required this.onContactChange,
    required this.index,
    required this.onDelete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return size.width > 425
        ? FormContactsIecItemDesktop(
            index: index,
            onDelete: onDelete,
            contact: contact,
            onContactChange: onContactChange,
          )
        : FormContactsIecItemMobile(
            index: index,
            onDelete: onDelete,
            contact: contact,
            onContactChange: onContactChange,
          );
  }
}

class FormContactsIecItemDesktop extends StatelessWidget {
  const FormContactsIecItemDesktop({
    Key? key,
    required this.index,
    required this.onDelete,
    required this.contact,
    required this.onContactChange,
  }) : super(key: key);

  final int index;
  final VoidCallback onDelete;
  final ContactModel contact;
  final void Function(ContactModel p1) onContactChange;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        if (index > 1) Divider(height: 32),
        Row(
          children: [
            Expanded(
              child: Text(
                'Contacto $index',
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            IconButton(
              icon: Icon(Icons.close),
              onPressed: onDelete,
            ),
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 1,
              child: FormGroup(
                label: AppLabel('Primer nombre'),
                input: AppInput(
                  initialValue: contact.firstName,
                  onChanged: (String value) {
                    contact.firstName = value;
                    onContactChange(contact);
                  },
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa el primer nombre del contacto.';
                    }
                    return null;
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 1,
              child: FormGroup(
                label: AppLabel('Segundo nombre'),
                input: AppInput(
                  initialValue: contact.middleName,
                  onChanged: (String value) {
                    contact.middleName = value;
                    onContactChange(contact);
                  },
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 1,
              child: FormGroup(
                label: AppLabel('Primer apellido'),
                input: AppInput(
                  initialValue: contact.lastName,
                  onChanged: (String value) {
                    contact.lastName = value;
                    onContactChange(contact);
                  },
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa el primer apellido del contacto.';
                    }
                    return null;
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 1,
              child: FormGroup(
                label: AppLabel('Segundo apellido'),
                input: AppInput(
                  initialValue: contact.secondLastName,
                  onChanged: (String value) {
                    contact.secondLastName = value;
                    onContactChange(contact);
                  },
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Parentesco'),
                input: AppInput(
                  initialValue: contact.relationship,
                  onChanged: (String value) {
                    contact.relationship = value;
                    onContactChange(contact);
                  },
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa el parentesco del contacto.';
                    }
                    return null;
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 1,
              child: FormGroup(
                label: AppLabel('Tipo'),
                input: SelectIdentificationsTypes(
                  short: true,
                  value: contact.identificationTypeId,
                  onChanged: (int value) {
                    contact.identificationTypeId = value;
                    onContactChange(contact);
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Documento de identidad'),
                input: AppInput(
                  initialValue: contact.identification,
                  onChanged: (String value) {
                    contact.identification = value;
                    onContactChange(contact);
                  },
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa el documento de identidad'
                          ' del contacto.';
                    }
                    return null;
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 1,
              child: FormGroup(
                label: AppLabel('Sexo'),
                input: SelectGenders(
                  value: contact.genderId,
                  onChanged: (int value) {
                    contact.genderId = value;
                    onContactChange(contact);
                  },
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 1,
              child: FormGroup(
                label: AppLabel('Edad'),
                input: AppInputNumber(
                  initialValue: contact.age,
                  onChanged: (int value) {
                    contact.age = value;
                    onContactChange(contact);
                  },
                  validator: (int? value) {
                    if (value == null || value == 0) {
                      return 'Por favor ingresa una edad.';
                    }
                    return null;
                  },
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: FormGroup(
                label: AppLabel(''),
                input: SelectAgesMeasures(
                  value: contact.ageMeasure,
                  onChanged: (String value) {
                    contact.ageMeasure = value;
                    onContactChange(contact);
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('EPS'),
                input: AppInput(
                  initialValue: contact.eps,
                  onChanged: (String value) {
                    contact.eps = value;
                    onContactChange(contact);
                  },
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa la EPS del contacto.';
                    }
                    return null;
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Teléfono'),
                input: AppInput(
                  initialValue: contact.primaryPhone,
                  onChanged: (String value) {
                    contact.primaryPhone = value;
                    onContactChange(contact);
                  },
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa el teléfono del contacto.';
                    }
                    return null;
                  },
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Fecha de contacto positivo'),
                input: AppInputDate(
                  initialValue: contact.contactDate,
                  onChanged: (DateTime? value) {
                    contact.contactDate = value;
                    onContactChange(contact);
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Fecha de toma de muestra'),
                input: AppInputDate(
                  initialValue: contact.sampleDate,
                  onChanged: (DateTime? value) {
                    contact.sampleDate = value;
                    onContactChange(contact);
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              flex: 2,
              child: FormGroup(
                label: AppLabel('Resultado de la muestra'),
                input: SelectSamplesResults(
                  value: contact.sampleResult,
                  onChanged: (String? value) {
                    contact.sampleResult = value;
                    onContactChange(contact);
                  },
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Dirección'),
          input: AppInput(
            initialValue: contact.address,
            onChanged: (String value) {
              contact.address = value;
              onContactChange(contact);
            },
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Por favor ingresa la dirección del contacto.';
              }
              return null;
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Síntomas'),
          input: SelectSymptoms(
            key: ValueKey(contact.symptoms.length),
            value: contact.symptoms,
            onConfirm: (List<int> symptoms) {
              contact.symptoms = symptoms;
              onContactChange(contact);
            },
          ),
        ),
        SizedBox(height: 20),
        FormSymptom(
          key: ValueKey('symptoms${contact.symptoms.length}'),
          onCreated: (symptom) {
            contact.symptoms.add(symptom.id);
            onContactChange(contact);
          },
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Antecedentes'),
          input: SelectComorbidities(
            key: ValueKey(contact.comorbidities.length),
            value: contact.comorbidities,
            onConfirm: (List<int> comorbidities) {
              contact.comorbidities = comorbidities;
              onContactChange(contact);
            },
          ),
        ),
        SizedBox(height: 20),
        FormComorbidity(
          key: ValueKey('comorbidities${contact.comorbidities.length}'),
          onCreated: (comorbidity) {
            contact.comorbidities.add(comorbidity.id);
            onContactChange(contact);
          },
        ),
      ],
    );
  }
}

class FormContactsIecItemMobile extends StatelessWidget {
  const FormContactsIecItemMobile({
    Key? key,
    required this.index,
    required this.onDelete,
    required this.contact,
    required this.onContactChange,
  }) : super(key: key);

  final int index;
  final VoidCallback onDelete;
  final ContactModel contact;
  final void Function(ContactModel p1) onContactChange;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        if (index > 1) Divider(height: 32),
        Row(
          children: [
            Expanded(
              child: Text(
                'Contacto $index',
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            IconButton(
              icon: Icon(Icons.close),
              onPressed: onDelete,
            ),
          ],
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Primer nombre'),
          input: AppInput(
            initialValue: contact.firstName,
            onChanged: (String value) {
              contact.firstName = value;
              onContactChange(contact);
            },
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Por favor ingresa el primer nombre del contacto.';
              }
              return null;
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Segundo nombre'),
          input: AppInput(
            initialValue: contact.middleName,
            onChanged: (String value) {
              contact.middleName = value;
              onContactChange(contact);
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Primer apellido'),
          input: AppInput(
            initialValue: contact.lastName,
            onChanged: (String value) {
              contact.lastName = value;
              onContactChange(contact);
            },
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Por favor ingresa el primer apellido del contacto.';
              }
              return null;
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Segundo apellido'),
          input: AppInput(
            initialValue: contact.secondLastName,
            onChanged: (String value) {
              contact.secondLastName = value;
              onContactChange(contact);
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Parentesco'),
          input: AppInput(
            initialValue: contact.relationship,
            onChanged: (String value) {
              contact.relationship = value;
              onContactChange(contact);
            },
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Por favor ingresa el parentesco del contacto.';
              }
              return null;
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Tipo'),
          input: SelectIdentificationsTypes(
            value: contact.identificationTypeId,
            onChanged: (int value) {
              contact.identificationTypeId = value;
              onContactChange(contact);
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Documento de identidad'),
          input: AppInput(
            initialValue: contact.identification,
            onChanged: (String value) {
              contact.identification = value;
              onContactChange(contact);
            },
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Por favor ingresa el documento de identidad'
                    ' del contacto.';
              }
              return null;
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Sexo'),
          input: SelectGenders(
            value: contact.genderId,
            onChanged: (int value) {
              contact.genderId = value;
              onContactChange(contact);
            },
          ),
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Flexible(
              child: FormGroup(
                label: AppLabel('Edad'),
                input: AppInputNumber(
                  initialValue: contact.age,
                  onChanged: (int value) {
                    contact.age = value;
                    onContactChange(contact);
                  },
                  validator: (int? value) {
                    if (value == null || value == 0) {
                      return 'Por favor ingresa una edad.';
                    }
                    return null;
                  },
                ),
              ),
            ),
            SizedBox(width: 24),
            Flexible(
              child: FormGroup(
                label: AppLabel(''),
                input: SelectAgesMeasures(
                  value: contact.ageMeasure,
                  onChanged: (String value) {
                    contact.ageMeasure = value;
                    onContactChange(contact);
                  },
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('EPS'),
          input: AppInput(
            initialValue: contact.eps,
            onChanged: (String value) {
              contact.eps = value;
              onContactChange(contact);
            },
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Por favor ingresa la EPS del contacto.';
              }
              return null;
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Teléfono'),
          input: AppInput(
            initialValue: contact.primaryPhone,
            onChanged: (String value) {
              contact.primaryPhone = value;
              onContactChange(contact);
            },
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Por favor ingresa el teléfono del contacto.';
              }
              return null;
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Fecha de contacto positivo'),
          input: AppInputDate(
            initialValue: contact.contactDate,
            onChanged: (DateTime? value) {
              contact.contactDate = value;
              onContactChange(contact);
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Fecha de toma de muestra'),
          input: AppInputDate(
            initialValue: contact.sampleDate,
            onChanged: (DateTime? value) {
              contact.sampleDate = value;
              onContactChange(contact);
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Resultado de la muestra'),
          input: SelectSamplesResults(
            value: contact.sampleResult,
            onChanged: (String? value) {
              contact.sampleResult = value;
              onContactChange(contact);
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Dirección'),
          input: AppInput(
            initialValue: contact.address,
            onChanged: (String value) {
              contact.address = value;
              onContactChange(contact);
            },
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Por favor ingresa la dirección del contacto.';
              }
              return null;
            },
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Síntomas'),
          input: SelectSymptoms(
            key: ValueKey(contact.symptoms.length),
            value: contact.symptoms,
            onConfirm: (List<int> symptoms) {
              contact.symptoms = symptoms;
              onContactChange(contact);
            },
          ),
        ),
        SizedBox(height: 20),
        FormSymptom(
          key: ValueKey('symptoms${contact.symptoms.length}'),
          onCreated: (symptom) {
            contact.symptoms.add(symptom.id);
            onContactChange(contact);
          },
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Antecedentes'),
          input: SelectComorbidities(
            key: ValueKey(contact.comorbidities.length),
            value: contact.comorbidities,
            onConfirm: (List<int> comorbidities) {
              contact.comorbidities = comorbidities;
              onContactChange(contact);
            },
          ),
        ),
        SizedBox(height: 20),
        FormComorbidity(
          key: ValueKey('comorbidities${contact.comorbidities.length}'),
          onCreated: (comorbidity) {
            contact.comorbidities.add(comorbidity.id);
            onContactChange(contact);
          },
        ),
      ],
    );
  }
}
