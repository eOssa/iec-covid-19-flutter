import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/models/contact_model.dart';
import 'package:iec_covid_19_flutter/ui/views/create_iec_view.dart';
import 'package:iec_covid_19_flutter/widgets/button_secondary.dart';
import 'package:iec_covid_19_flutter/widgets/form_contacts_iec/form_contacts_iec_item.dart';

class FormContactsIec extends StatelessWidget {
  final int id;

  FormContactsIec({required this.id});

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(createIecProvider.find('iecs.$id'));
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ...ctrl.contacts.asMap().map<int, Widget>((index, contact) {
              return MapEntry(
                index,
                FormContactsIecItem(
                  contact,
                  key: ValueKey(contact.id ?? contact.name),
                  index: index + 1,
                  onContactChange: (contact) => ctrl.updateContact(contact),
                  onDelete: () => ctrl.deleteContact(contact),
                ),
              );
            }).values,
            Container(
              margin: EdgeInsets.only(top: 8),
              width: double.infinity,
              child: ButtonSecondary(
                child: Text('+ Añadir contacto estrecho'),
                onPressed: () {
                  ContactModel contact = ContactModel(
                    address: ctrl.address ?? '',
                    age: 0,
                    ageMeasure: 'year',
                    comorbidities: [],
                    contactDate: ctrl.symptomDate ?? DateTime.now(),
                    eps: ctrl.eps ?? '',
                    firstName: '',
                    genderId: 1,
                    id: -Random().nextInt(100),
                    identification: '',
                    identificationTypeId: 1,
                    lastName: '',
                    middleName: '',
                    primaryPhone: ctrl.primaryPhone ?? '',
                    relationship: '',
                    sampleDate: null,
                    sampleResult: '',
                    secondLastName: '',
                    symptoms: [],
                  );
                  ctrl.addContact(contact);
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
