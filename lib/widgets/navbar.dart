import 'package:flutter/material.dart';
import 'profile_image_dropdown.dart';
import 'package:flutter_meedu/router.dart' as router;

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      color: Color(0xff1f2937),
      padding: EdgeInsets.symmetric(horizontal: 32),
      width: double.infinity,
      height: 64,
      child: Row(
        children: [
          Flexible(
            flex: 5,
            child: Container(
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: InkWell(
                      child: Image.asset('images/workflow-logo.png'),
                      onTap: () {
                        router.popUntil(ModalRoute.withName('/'));
                      },
                    ),
                  ),
                  if (size.width > 425)
                    Container(
                      margin: EdgeInsets.only(left: 24),
                      padding:
                          EdgeInsets.symmetric(horizontal: 12, vertical: 16),
                      child: TextButton(
                        child: Text(
                          'IECs',
                          style: TextStyle(
                              color: Colors.grey[350],
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                        ),
                        onPressed: () {
                          if (router.canPop()) {
                            router.pushReplacementNamed('/iecs');
                          } else {
                            router.pushNamed('/iecs');
                          }
                        },
                      ),
                    ),
                  // if (size.width > 425)
                  //   Container(
                  //     padding:
                  //         EdgeInsets.symmetric(horizontal: 12, vertical: 16),
                  //     margin: EdgeInsets.only(left: 16),
                  //     child: TextButton(
                  //       child: Text(
                  //         'Nueva IEC',
                  //         style: TextStyle(
                  //             color: Colors.grey[350],
                  //             fontSize: 14,
                  //             fontWeight: FontWeight.w500),
                  //       ),
                  //       onPressed: () {
                  //         router.pushNamed('/create-iec');
                  //       },
                  //     ),
                  //   ),
                ],
              ),
            ),
          ),
          Flexible(
            flex: 0,
            child: ProfileImageDropdown(),
          )
        ],
      ),
    );
  }
}
