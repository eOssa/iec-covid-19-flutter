import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/medications_controller.dart';
import 'package:iec_covid_19_flutter/models/medication_model.dart';
import 'package:iec_covid_19_flutter/widgets/app_multiselect.dart';

final medicationsProvider = SimpleProvider((ref) => MedicationsController());

class SelectMedications extends StatelessWidget {
  final List<int>? value;
  final void Function(List<int>) onConfirm;

  const SelectMedications({Key? key, this.value, required this.onConfirm})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(medicationsProvider);
        return AppMultiselect<String?>(
          buttonText: Text('Selecciona los medicamentos'),
          value: value?.map<String>((int val) => val.toString()).toList(),
          onConfirm: (List<String?> values) {
            onConfirm(values.map<int>((String? v) => int.parse(v!)).toList());
          },
          items: ctrl.medications.map<Map<String, dynamic>>(
            (MedicationModel medication) {
              return {'id': medication.id.toString(), 'name': medication.name};
            },
          ).toList(),
        );
      },
    );
  }
}
