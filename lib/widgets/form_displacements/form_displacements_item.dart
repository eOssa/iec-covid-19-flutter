import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/widgets/app_input.dart';
import 'package:iec_covid_19_flutter/widgets/app_input_date.dart';
import 'package:iec_covid_19_flutter/widgets/app_label.dart';
import 'package:iec_covid_19_flutter/widgets/button_danger.dart';
import 'package:iec_covid_19_flutter/widgets/form_group.dart';

class FormDisplacementsItem extends StatelessWidget {
  final int index;
  final String? city;
  final String? country;
  final DateTime? endDate;
  final DateTime? startDate;
  final VoidCallback onDelete;
  final void Function(String)? onCityChanged;
  final void Function(String)? onCountryChanged;
  final void Function(DateTime?)? onEndDateChanged;
  final void Function(DateTime?)? onStartDateChanged;

  const FormDisplacementsItem({
    Key? key,
    required this.index,
    required this.onDelete,
    this.city,
    this.country,
    this.endDate,
    this.startDate,
    this.onCityChanged,
    this.onCountryChanged,
    this.onEndDateChanged,
    this.onStartDateChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.only(top: 16),
      child: size.width > 425
          ? FormDisplacementsItemBodyDesktop(
              index: index,
              country: country,
              onCountryChanged: onCountryChanged,
              city: city,
              onCityChanged: onCityChanged,
              startDate: startDate,
              onStartDateChanged: onStartDateChanged,
              endDate: endDate,
              onEndDateChanged: onEndDateChanged,
              onDelete: onDelete,
            )
          : FormDisplacementsItemBodyMobile(
              index: index,
              country: country,
              onCountryChanged: onCountryChanged,
              city: city,
              onCityChanged: onCityChanged,
              startDate: startDate,
              onStartDateChanged: onStartDateChanged,
              endDate: endDate,
              onEndDateChanged: onEndDateChanged,
              onDelete: onDelete,
            ),
    );
  }
}

class FormDisplacementsItemBodyDesktop extends StatelessWidget {
  const FormDisplacementsItemBodyDesktop({
    Key? key,
    required this.index,
    required this.country,
    required this.onCountryChanged,
    required this.city,
    required this.onCityChanged,
    required this.startDate,
    required this.onStartDateChanged,
    required this.endDate,
    required this.onEndDateChanged,
    required this.onDelete,
  }) : super(key: key);

  final int index;
  final String? country;
  final void Function(String p1)? onCountryChanged;
  final String? city;
  final void Function(String p1)? onCityChanged;
  final DateTime? startDate;
  final void Function(DateTime? p1)? onStartDateChanged;
  final DateTime? endDate;
  final void Function(DateTime? p1)? onEndDateChanged;
  final VoidCallback onDelete;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
          flex: 2,
          child: FormGroup(
            label: AppLabel('País $index'),
            input: AppInput(
              initialValue: country,
              onChanged: onCountryChanged,
            ),
          ),
        ),
        SizedBox(width: 24),
        Flexible(
          flex: 2,
          child: FormGroup(
            label: AppLabel('Ciudad'),
            input: AppInput(
              initialValue: city,
              onChanged: onCityChanged,
            ),
          ),
        ),
        SizedBox(width: 24),
        Flexible(
          flex: 2,
          child: FormGroup(
            label: AppLabel('Desde'),
            input: AppInputDate(
              initialValue: startDate,
              onChanged: onStartDateChanged,
            ),
          ),
        ),
        SizedBox(width: 24),
        Flexible(
          flex: 2,
          child: FormGroup(
            label: AppLabel('Hasta'),
            input: AppInputDate(
              initialValue: endDate,
              onChanged: onEndDateChanged,
            ),
          ),
        ),
        SizedBox(width: 24),
        Flexible(
          child: ButtonDanger(
            child: Icon(Icons.delete),
            onPressed: onDelete,
          ),
        )
      ],
    );
  }
}

class FormDisplacementsItemBodyMobile extends StatelessWidget {
  const FormDisplacementsItemBodyMobile({
    Key? key,
    required this.index,
    required this.country,
    required this.onCountryChanged,
    required this.city,
    required this.onCityChanged,
    required this.startDate,
    required this.onStartDateChanged,
    required this.endDate,
    required this.onEndDateChanged,
    required this.onDelete,
  }) : super(key: key);

  final int index;
  final String? country;
  final void Function(String p1)? onCountryChanged;
  final String? city;
  final void Function(String p1)? onCityChanged;
  final DateTime? startDate;
  final void Function(DateTime? p1)? onStartDateChanged;
  final DateTime? endDate;
  final void Function(DateTime? p1)? onEndDateChanged;
  final VoidCallback onDelete;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Desplazamiento $index'),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('País'),
          input: AppInput(
            initialValue: country,
            onChanged: onCountryChanged,
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Ciudad'),
          input: AppInput(
            initialValue: city,
            onChanged: onCityChanged,
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Desde'),
          input: AppInputDate(
            initialValue: startDate,
            onChanged: onStartDateChanged,
          ),
        ),
        SizedBox(height: 20),
        FormGroup(
          label: AppLabel('Hasta'),
          input: AppInputDate(
            initialValue: endDate,
            onChanged: onEndDateChanged,
          ),
        ),
        SizedBox(height: 20),
        ButtonDanger(
          child: Text('Quitar desplazamiento $index'),
          onPressed: onDelete,
        )
      ],
    );
  }
}
