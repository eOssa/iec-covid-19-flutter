import 'package:flutter/material.dart';
import 'package:iec_covid_19_flutter/widgets/button_secondary.dart';
import 'package:iec_covid_19_flutter/widgets/form_displacements/form_displacements_item.dart';

class FormDisplacements extends StatelessWidget {
  final List<Map<String, dynamic>> displacements;
  final VoidCallback onAddDisplacement;
  final void Function(dynamic, String) onCityChanged;
  final void Function(dynamic, String) onCountryChanged;
  final void Function(dynamic, DateTime?) onEndDateChanged;
  final void Function(dynamic, DateTime?) onStartDateChanged;
  final void Function(dynamic) onDelete;

  const FormDisplacements({
    Key? key,
    required this.displacements,
    required this.onAddDisplacement,
    required this.onCityChanged,
    required this.onCountryChanged,
    required this.onEndDateChanged,
    required this.onStartDateChanged,
    required this.onDelete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int counter = 0;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...displacements.map<Widget>((Map<String, dynamic> displacement) {
          return FormDisplacementsItem(
            key: ValueKey(displacement['id']),
            index: ++counter,
            city: displacement['city'],
            country: displacement['country'],
            endDate: displacement['endDate'],
            startDate: displacement['startDate'],
            onCityChanged: (String city) {
              onCityChanged(displacement['id'], city);
            },
            onCountryChanged: (String country) {
              onCountryChanged(displacement['id'], country);
            },
            onEndDateChanged: (DateTime? endDate) {
              onEndDateChanged(displacement['id'], endDate);
            },
            onStartDateChanged: (DateTime? startDate) {
              onStartDateChanged(displacement['id'], startDate);
            },
            onDelete: () {
              onDelete(displacement['id']);
            },
          );
        }),
        Container(
          margin: EdgeInsets.only(top: 8),
          child: ButtonSecondary(
            child: Text('+ Añadir desplazamiento'),
            onPressed: onAddDisplacement,
          ),
        ),
      ],
    );
  }
}
