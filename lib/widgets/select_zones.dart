import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/zones_controller.dart';
import 'package:iec_covid_19_flutter/widgets/app_select.dart';

final zonesProvider = SimpleProvider((ref) => ZonesController());

class SelectZones extends StatelessWidget {
  final int value;
  final void Function(int) onChanged;

  const SelectZones({Key? key, required this.value, required this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(zonesProvider);
        return AppSelect<int>(
          value: value,
          onChanged: (v) => onChanged(v!),
          items: ctrl.zones.map((value) => value.toMap()).toList(),
        );
      },
    );
  }
}
