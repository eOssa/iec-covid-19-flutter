import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/comorbidities_controller.dart';
import 'package:iec_covid_19_flutter/widgets/app_multiselect.dart';

final comorbiditiesProvider =
    SimpleProvider((ref) => ComorbiditiesController());

class SelectComorbidities extends StatelessWidget {
  final List<int>? value;
  final void Function(List<int>) onConfirm;

  const SelectComorbidities({Key? key, this.value, required this.onConfirm})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(comorbiditiesProvider);
        return AppMultiselect<String?>(
          value: value?.map<String?>((int val) => val.toString()).toList(),
          onConfirm: (List<String?> values) {
            onConfirm(
              values
                  .map<int>((String? val) => int.parse(val.toString()))
                  .toList(),
            );
          },
          items: ctrl.comorbidities.map((value) {
            return {'id': value.id.toString(), 'name': value.name};
          }).toList(),
        );
      },
    );
  }
}
