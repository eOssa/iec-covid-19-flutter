import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  final Widget child;
  final VoidCallback onPressed;
  final Color? backgroundColor;
  final Color textColor;
  final BorderSide? side;

  const AppButton({
    Key? key,
    required this.onPressed,
    required this.child,
    this.backgroundColor,
    this.textColor: Colors.white,
    this.side,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: child,
      onPressed: onPressed,
      style: TextButton.styleFrom(
        primary: textColor,
        backgroundColor: backgroundColor,
        padding: EdgeInsets.symmetric(horizontal: 28, vertical: 16),
        side: side,
      ),
    );
  }
}
