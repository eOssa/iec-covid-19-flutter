import 'package:flutter/material.dart';

class FormPanel extends StatelessWidget {
  final String text;
  final Widget? content;

  const FormPanel({Key? key, required this.text, this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          text,
          style: Theme.of(context).textTheme.headline6,
        ),
        Container(child: content),
      ],
    );
  }
}
