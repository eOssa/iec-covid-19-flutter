import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/controllers/symptoms_controller.dart';
import 'package:iec_covid_19_flutter/models/symptom_model.dart';
import 'package:iec_covid_19_flutter/widgets/app_multiselect.dart';

final symptomsProvider = SimpleProvider((ref) => SymptomsController());

class SelectSymptoms extends StatelessWidget {
  final List<int>? value;
  final void Function(List<int>) onConfirm;

  const SelectSymptoms({Key? key, this.value, required this.onConfirm})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final ctrl = ref.watch(symptomsProvider);
        return AppMultiselect<String?>(
          value: value?.map<String?>((int val) => val.toString()).toList(),
          onConfirm: (List<String?> values) {
            onConfirm(values.map((String? v) => int.parse(v!)).toList());
          },
          items: ctrl.symptoms.map<Map<String, dynamic>>(
            (SymptomModel symptom) {
              return {'id': symptom.id.toString(), 'name': symptom.name};
            },
          ).toList(),
        );
      },
    );
  }
}
