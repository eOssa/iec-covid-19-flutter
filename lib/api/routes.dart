class Routes {
  static final host = 'https://iec-covid-19.herokuapp.com/api';

  static final iecReport = '$host/iecs/report';

  static final iecsReport = '$host/iecs/folders-report';

  static final contactsReport = '$host/iecs/contacts/report';
}
