import 'package:iec_covid_19_flutter/models/model.dart';

class RegionModel extends Model {
  String name;

  RegionModel({int id: 0, this.name: ''}) : super('regions', id: id);

  factory RegionModel.fromJson(Map<String, dynamic> json) {
    return RegionModel(
      id: json['id'],
      name: json['name'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }
}
