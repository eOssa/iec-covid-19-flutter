import 'package:iec_covid_19_flutter/models/model.dart';

class ComorbidityModel extends Model {
  String name;

  ComorbidityModel({int id: 0, this.name: ''}) : super('comorbidities', id: id);

  factory ComorbidityModel.fromJson(Map<String, dynamic> json) {
    return ComorbidityModel(
      id: json['id'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }
}
