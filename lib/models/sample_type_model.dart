import 'package:iec_covid_19_flutter/models/model.dart';

class SampleTypeModel extends Model {
  String name;

  SampleTypeModel({int id: 0, this.name: ''}) : super('samples_types', id: id);

  factory SampleTypeModel.fromJson(Map<String, dynamic> json) {
    return SampleTypeModel(
      id: json['id'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }
}
