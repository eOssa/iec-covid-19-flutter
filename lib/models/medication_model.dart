import 'package:iec_covid_19_flutter/models/model.dart';

class MedicationModel extends Model {
  String name;

  MedicationModel({int id: 0, this.name: ''}) : super('medications', id: id);

  factory MedicationModel.fromJson(Map<String, dynamic> json) {
    return MedicationModel(
      id: json['id'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }
}
