import 'package:iec_covid_19_flutter/helpers/date.dart';

class ContactModel {
  String address;
  int? age;
  String? ageMeasure;
  DateTime? birthdate;
  int? cityId;
  List<int> comorbidities;
  DateTime? contactDate;
  List<Map<String, dynamic>>? displacements;
  String eps;
  int genderId;
  List<Map<String, dynamic>>? hospitalizations;
  int? id;
  String identification;
  int identificationTypeId;
  String firstName;
  String lastName;
  String? middleName;
  String? nationality;
  String? neighborhood;
  String? occupation;
  int? pregnancyWeeks;
  String primaryPhone;
  int? regionId;
  String? relationship;
  DateTime? sampleDate;
  String? sampleResult;
  String? secondaryPhone;
  String? secondLastName;
  List<int> symptoms;
  int? zoneId;

  ContactModel({
    required this.address,
    this.age,
    this.ageMeasure,
    this.birthdate,
    this.cityId,
    required this.comorbidities,
    this.contactDate,
    this.displacements,
    required this.eps,
    required this.firstName,
    required this.genderId,
    this.hospitalizations,
    this.id,
    required this.identification,
    required this.identificationTypeId,
    required this.lastName,
    this.middleName,
    this.nationality,
    this.neighborhood,
    this.occupation,
    this.pregnancyWeeks,
    required this.primaryPhone,
    this.relationship,
    this.regionId,
    this.sampleDate,
    this.sampleResult,
    this.secondaryPhone,
    this.secondLastName,
    required this.symptoms,
    this.zoneId,
  });

  String get firstNames => '$firstName ${middleName ?? ''}'.trim();
  String get lastNames => '$lastName ${secondLastName ?? ''}'.trim();
  String get name => '$firstNames $lastNames'.trim();

  factory ContactModel.fromJson(Map<String, dynamic> json) {
    return ContactModel(
      address: json['address'],
      age: json['age'],
      ageMeasure: json['age_measure'],
      birthdate: DateHelper.parse(json['birthdate']),
      cityId: json['city_id'],
      comorbidities: json['comorbidities'].map<int>((v) => v as int).toList(),
      contactDate: DateHelper.parse(json['contact_date']),
      displacements: json['displacements']?.map<Map<String, dynamic>>((v) {
        final map = Map<String, dynamic>.from(v as Map<String, dynamic>);
        map['startDate'] = DateHelper.parse(map['start_date']);
        map['endDate'] = DateHelper.parse(map['end_date']);
        return map;
      }).toList(),
      eps: json['eps'],
      firstName: json['first_name'],
      genderId: json['gender_id'],
      hospitalizations:
          json['hospitalizations']?.map<Map<String, dynamic>>((v) {
        final map = Map<String, dynamic>.from(v as Map<String, dynamic>);
        map['date'] = DateHelper.parse(map['date']);
        return map;
      }).toList(),
      id: json['id'],
      identification: json['identification'],
      identificationTypeId: json['identification_type_id'],
      lastName: json['last_name'],
      middleName: json['middle_name'],
      nationality: json['nationality'],
      neighborhood: json['neighborhood'],
      occupation: json['occupation'],
      pregnancyWeeks: json['pregnancy_weeks'],
      primaryPhone: json['primary_phone'],
      regionId: json['region_id'],
      relationship: json['relationship'],
      sampleDate: DateHelper.parse(json['sample_date']),
      sampleResult: json['sample_result'],
      secondaryPhone: json['secondary_phone'],
      secondLastName: json['second_last_name'],
      symptoms: json['symptoms']?.map<int>((v) => v as int).toList(),
      zoneId: json['zone_id'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'address': address,
      'age': age,
      'age_measure': ageMeasure,
      'birthdate': DateHelper.format(birthdate),
      'city_id': cityId,
      'comorbidities': comorbidities,
      'contact_date': DateHelper.format(contactDate),
      'displacements': displacements?.map((value) {
        final Map<String, dynamic> map = Map.from(value);
        map['start_date'] = DateHelper.format(value['startDate']);
        map['end_date'] = DateHelper.format(value['endDate']);
        map..remove('startDate')..remove('endDate');
        return map;
      }).toList(),
      'eps': eps,
      'first_name': firstName,
      'gender_id': genderId,
      'hospitalizations': hospitalizations?.map((value) {
        final Map<String, dynamic> map = Map.from(value);
        map['date'] = DateHelper.format(value['date']);
        return map;
      }).toList(),
      'identification': identification,
      'identification_type_id': identificationTypeId,
      'last_name': lastName,
      'middle_name': middleName,
      'name': name,
      'nationality': nationality,
      'neighborhood': neighborhood,
      'occupation': occupation,
      'pregnancy_weeks': pregnancyWeeks,
      'primary_phone': primaryPhone,
      'region_id': regionId,
      'relationship': relationship,
      'sample_date': DateHelper.format(sampleDate),
      'sample_result': sampleResult,
      'secondary_phone': secondaryPhone,
      'second_last_name': secondLastName,
      'symptoms': symptoms,
      'zone_id': zoneId,
    };
  }

  @override
  String toString() {
    return '''
ContactModel{
  address: $address,
  age: $age,
  ageMeasure: $ageMeasure,
  birthdate: $birthdate,
  cityId: $cityId,
  comorbidities: $comorbidities,
  contactDate: $contactDate,
  displacements: $displacements,
  eps: $eps,
  gender: $genderId,
  hospitalizations: $hospitalizations,
  id: $id,
  identification: $identification,
  identificationTypeId: $identificationTypeId,
  first_name: $firstName,
  last_name: $lastName,
  middle_name: $middleName,
  nationality: $nationality,
  neighborhood: $neighborhood,
  occupation: $occupation,
  pregnancyWeeks: $pregnancyWeeks,
  primaryPhone: $primaryPhone,
  regionId: $regionId,
  relationship: $relationship,
  sampleDate: $sampleDate,
  sampleResult: $sampleResult,
  secondaryPhone: $secondaryPhone,
  second_last_name: $secondLastName,
  symptoms: $symptoms,
  zone: $zoneId',
}''';
  }
}
