import 'package:iec_covid_19_flutter/models/model.dart';

class ExamModel extends Model {
  String name;

  ExamModel({int id: 0, this.name: ''}) : super('exams', id: id);

  factory ExamModel.fromJson(Map<String, dynamic> json) {
    return ExamModel(
      id: json['id'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }
}
