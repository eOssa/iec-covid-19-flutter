import 'package:iec_covid_19_flutter/models/model.dart';

class IdentificationTypeModel extends Model {
  String name;

  IdentificationTypeModel({int id: 0, this.name: ''})
      : super('identifications_types', id: id);

  factory IdentificationTypeModel.fromJson(Map<String, dynamic> json) {
    return IdentificationTypeModel(
      id: json['id'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }
}
