import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/helpers/date.dart';
import 'package:iec_covid_19_flutter/models/city_model.dart';
import 'package:iec_covid_19_flutter/models/comorbidity_model.dart';
import 'package:iec_covid_19_flutter/models/contact_model.dart';
import 'package:iec_covid_19_flutter/models/gender_model.dart';
import 'package:iec_covid_19_flutter/models/identification_type_model.dart';
import 'package:iec_covid_19_flutter/models/medication_model.dart';
import 'package:iec_covid_19_flutter/models/model.dart';
import 'package:iec_covid_19_flutter/models/region_model.dart';
import 'package:iec_covid_19_flutter/models/symptom_model.dart';

class IecModel extends Model {
  ContactModel? contact;
  List<ContactModel>? contacts;
  DateTime? date;
  bool? effective;
  int? examId;
  List<int>? medications;
  String? nursingNote;
  String? positiveContactPlace;
  DateTime? sampleDate;
  String? sampleLaboratory;
  int? sampleTypeId;
  DateTime? symptomDate;
  bool? withPositiveContact;

  IecModel({
    int id: 0,
    this.contact,
    this.contacts,
    this.date,
    this.effective,
    this.examId,
    this.medications,
    this.nursingNote,
    this.positiveContactPlace,
    this.sampleDate,
    this.sampleLaboratory,
    this.sampleTypeId,
    this.symptomDate,
    this.withPositiveContact,
  }) : super('iecs', id: id);

  factory IecModel.fromJson(Map<String, dynamic> json) {
    return IecModel(
      contact: ContactModel.fromJson(json['contact']),
      contacts: json['contacts']
          .map<ContactModel>(
              (dynamic contact) => ContactModel.fromJson(contact))
          .toList(),
      date: DateHelper.parse(json['date']),
      effective: json['effective'],
      examId: json['exam_id'],
      id: json['id'],
      medications: json['medications']?.map<int>((v) => v as int).toList(),
      nursingNote: json['nursing_note'],
      positiveContactPlace: json['positive_contact_place'],
      sampleDate: DateHelper.parse(json['sample_date']),
      sampleLaboratory: json['sample_laboratory'],
      sampleTypeId: json['sample_type_id'],
      symptomDate: DateHelper.parse(json['symptom_date']),
      withPositiveContact: json['with_positive_contact'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'contact': contact?.toMap(),
      'contacts': contacts?.map((contact) => contact.toMap()).toList(),
      'date': DateHelper.format(date, format: 'yyyy-MM-dd HH:mm'),
      'effective': effective,
      'exam_id': examId,
      'id': id,
      'medications': medications,
      'nursing_note': nursingNote,
      'positive_contact_place': positiveContactPlace,
      'sample_date': DateHelper.format(sampleDate),
      'sample_laboratory': sampleLaboratory,
      'sample_type_id': sampleTypeId,
      'symptom_date': DateHelper.format(symptomDate),
      'with_positive_contact': withPositiveContact,
    };
  }

  Future<Map<String, dynamic>> toContactsReportMap() async {
    final db = Get.i.find<DatabaseConnector>();
    final Map<String, String> relationshipTypes = {
      'abuela': 'Familiar',
      'abuelo': 'Familiar',
      'bisabuela': 'Familiar',
      'bisabuelo': 'Familiar',
      'bisnieta': 'Familiar',
      'bisnieto': 'Familiar',
      'cuñada': 'Familiar',
      'cuñado': 'Familiar',
      'esposa': 'Familiar',
      'esposo': 'Familiar',
      'hermana': 'Familiar',
      'hermano': 'Familiar',
      'hija': 'Familiar',
      'hijo': 'Familiar',
      'madrastra': 'Familiar',
      'madre': 'Familiar',
      'mama': 'Familiar',
      'mamá': 'Familiar',
      'nieta': 'Familiar',
      'nieto': 'Familiar',
      'padrastro': 'Familiar',
      'padre': 'Familiar',
      'papá': 'Familiar',
      'papa': 'Familiar',
      'prima': 'Familiar',
      'primo': 'Familiar',
      'suegra': 'Familiar',
      'suegro': 'Familiar',
      'tía': 'Familiar',
      'tío': 'Familiar',
      'tia': 'Familiar',
      'tio': 'Familiar',
      'yerno': 'Familiar',
      'yerna': 'Familiar',
      'colaborador': 'Laboral',
      'colaboradora': 'Laboral',
      'compañera': 'Laboral',
      'compañera de trabajo': 'Laboral',
      'compañero': 'Laboral',
      'compañero de trabajo': 'Laboral',
      'coordinador': 'Laboral',
      'coordinadora': 'Laboral',
      'jefa': 'Laboral',
      'jefe': 'Laboral',
      'subordinada': 'Laboral',
      'subordinado': 'Laboral',
      'supervisor': 'Laboral',
      'supervisora': 'Laboral',
      'trabajador': 'Laboral',
      'trabajadora': 'Laboral',
    };
    return {
      'contact': {'name': contact!.name},
      'contacts': await Future.wait<Map<String, dynamic>>(contacts!.map(
        (ContactModel contact) async {
          final gender = await db.find(
            Get.i.find<GenderModel>().table,
            contact.genderId,
          );
          final identificationType = await db.find(
            Get.i.find<IdentificationTypeModel>().table,
            contact.identificationTypeId,
          );
          return {
            'address': contact.address,
            'age': contact.age,
            'age_measure': contact.ageMeasure,
            'contact_date': DateHelper.format(contact.contactDate),
            'eps': contact.eps,
            'first_name': contact.firstName,
            'gender': gender!['name'],
            'identification_type': identificationType!['name'],
            'identification': contact.identification,
            'last_name': contact.lastName,
            'middle_name': contact.middleName,
            'primary_phone': contact.primaryPhone,
            'relationship_type':
                relationshipTypes[contact.relationship?.toLowerCase()] ??
                    'Social',
            'secondary_phone': contact.secondaryPhone,
            'sample_date': DateHelper.format(contact.sampleDate),
            'sample_result': contact.sampleResult,
            'second_last_name': contact.secondLastName,
            'symptoms_ids': contact.symptoms,
          };
        },
      )),
      'iec': {'date': DateHelper.format(date)},
      'user': {'city': 'Pereira'},
    };
  }

  Future<Map<String, dynamic>> toIecReportMap() async {
    final db = Get.i.find<DatabaseConnector>();
    final identificationType = await db.find(
      Get.i.find<IdentificationTypeModel>().table,
      contact!.identificationTypeId,
    );
    final city = await db.find(Get.i.find<CityModel>().table, contact!.cityId!);
    final region = await db.find(
      Get.i.find<RegionModel>().table,
      contact!.regionId!,
    );
    final medicationsList = await db.findMany(
      Get.i.find<MedicationModel>().table,
      medications!,
    );
    final comorbidities = await db.findMany(
      Get.i.find<ComorbidityModel>().table,
      contact!.comorbidities,
    );
    final symptoms = await db.findMany(
      Get.i.find<SymptomModel>().table,
      contact!.symptoms,
    );
    return {
      'contact': {
        'address': contact!.address,
        'birthdate': DateHelper.format(contact!.birthdate),
        'city': city!['name'],
        'comorbidities': comorbidities,
        'displacements': contact!.displacements?.map((v) {
          final map = Map<String, dynamic>.from(v);
          map['startDate'] = map['start_date'];
          map['endDate'] = map['end_date'];
          return map;
        }).toList(),
        'eps': contact!.eps,
        'hospitalizations': contact!.hospitalizations?.map((v) {
          final map = Map<String, dynamic>.from(v);
          map['date'] = DateHelper.format(map['date']);
          return map;
        }).toList(),
        'identification': contact!.identification,
        'identification_type': identificationType!['name'],
        'is_female': contact!.genderId == 1 ? 1 : 0,
        'is_male': contact!.genderId == 2 ? 1 : 0,
        'live_in_urban': contact!.zoneId == 2 ? 1 : 0,
        'live_in_rural': contact!.zoneId == 1 ? 1 : 0,
        'name': contact!.name,
        'nationality': contact!.nationality,
        'neighborhood': contact!.neighborhood,
        'occupation': contact!.occupation,
        'primary_phone': contact!.primaryPhone,
        'pregnancy_weeks': contact!.pregnancyWeeks,
        'region': region!['name'],
        'secondary_phone': contact!.secondaryPhone,
        'symptoms': symptoms,
      },
      'contacts': await Future.wait<Map<String, dynamic>>(contacts!.map(
        (ContactModel contact) async {
          final identificationType = await db.find(
            Get.i.find<IdentificationTypeModel>().table,
            contact.identificationTypeId,
          );
          final comorbidities = await db.findMany(
            Get.i.find<ComorbidityModel>().table,
            contact.comorbidities,
          );
          final symptoms = await db.findMany(
            Get.i.find<SymptomModel>().table,
            contact.symptoms,
          );
          return {
            'address': contact.address,
            'age_with_measure': getAgeWithMeasure(
              contact.age!,
              contact.ageMeasure!,
            ),
            'comorbidities': comorbidities.map((v) => v['name']).join(', '),
            'contact_date': DateHelper.format(contact.contactDate),
            'eps': contact.eps,
            'identification_type': identificationType!['name'],
            'identification': contact.identification,
            'name': contact.name,
            'primary_phone': contact.primaryPhone,
            'relationship': contact.relationship,
            'secondary_phone': contact.secondaryPhone,
            'sample_date': DateHelper.format(contact.sampleDate),
            'sample_result': contact.sampleResult,
            'symptoms': symptoms.map((v) => v['name']).join(', '),
          };
        },
      )),
      'iec': {
        'date': DateHelper.format(date),
        'exam_id': examId,
        'is_effective': effective! ? 1 : 0,
        'medications': medicationsList.map((v) => v['name']).join(', '),
        'nursing_note': nursingNote,
        'positive_contact_place': positiveContactPlace,
        'sample_date': DateHelper.format(sampleDate),
        'sample_laboratory': sampleLaboratory,
        'sample_type_id': sampleTypeId,
        'symptom_date': DateHelper.format(symptomDate),
        'with_positive_contact': withPositiveContact! ? 1 : 0,
      },
      'user': {
        'city': 'Pereira',
        'name': 'Daniela Holguín Yate',
        'phone': '3205247701',
        'region': 'Risaralda',
      },
    };
  }

  String getAgeWithMeasure(int age, String measure) {
    Map<String, String> measures = {
      'year': age == 1 ? 'Año' : 'Años',
      'month': age == 1 ? 'Mes' : 'Meses',
      'day': age == 1 ? 'Día' : 'Días',
    };
    return '$age ${measures[measure]}';
  }

  @override
  String toString() {
    return '''
IecModel{
  contact: $contact
  contacts: $contacts
  id: $id,
  effective: $effective,
  medications: $medications,
}''';
  }
}
