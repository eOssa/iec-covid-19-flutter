import 'package:iec_covid_19_flutter/models/model.dart';

class CityModel extends Model {
  int regionId;
  String name;

  CityModel({int id: 0, this.regionId: 0, this.name: ''})
      : super('cities', id: id);

  factory CityModel.fromJson(Map<String, dynamic> json) {
    return CityModel(
      id: json['id'],
      regionId: json['region_id'],
      name: json['name'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'region_id': regionId,
      'name': name,
    };
  }

  @override
  String toString() {
    return 'CityModel {id: $id, name: $name}';
  }
}
