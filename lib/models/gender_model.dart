import 'package:iec_covid_19_flutter/models/model.dart';

class GenderModel extends Model {
  String name;

  GenderModel({int id: 0, this.name: ''}) : super('genders', id: id);

  factory GenderModel.fromJson(Map<String, dynamic> json) {
    return GenderModel(
      id: json['id'],
      name: json['name'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }
}
