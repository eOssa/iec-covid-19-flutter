import 'package:iec_covid_19_flutter/models/model.dart';

class ZoneModel extends Model {
  String name;

  ZoneModel({int id: 0, this.name: ''}) : super('zones', id: id);

  factory ZoneModel.fromJson(Map<String, dynamic> json) {
    return ZoneModel(
      id: json['id'],
      name: json['name'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }
}
