import 'package:iec_covid_19_flutter/models/model.dart';

class SymptomModel extends Model {
  String name;

  SymptomModel({int id: 0, this.name: ''}) : super('symptoms', id: id);

  factory SymptomModel.fromJson(Map<String, dynamic> json) {
    return SymptomModel(
      id: json['id'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }
}
