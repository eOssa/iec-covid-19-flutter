import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/db/query_builder.dart';
import 'package:meedu/meedu.dart';

abstract class Model {
  int id;
  final String table;

  Model(this.table, {required this.id});

  Future<bool> insert() async {
    id = await Get.i.find<DatabaseConnector>().insert(table, prepareForSave());
    return id != 0;
  }

  Future<bool> save() async {
    if (id == 0) {
      return await insert();
    }
    return await update();
  }

  Future<bool> update() async {
    id = await Get.i
        .find<DatabaseConnector>()
        .update(table, id, prepareForSave());
    return id != 0;
  }

  QueryBuilder query() {
    return QueryBuilder(table);
  }

  Map<String, dynamic> toMap();
  Map<String, dynamic> prepareForSave() {
    return toMap()..remove('id');
  }
}
