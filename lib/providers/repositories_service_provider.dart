import 'package:iec_covid_19_flutter/repositories/city_repository.dart';
import 'package:iec_covid_19_flutter/repositories/city_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/comorbidity_repository.dart';
import 'package:iec_covid_19_flutter/repositories/comorbidity_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/contacts_report_api_repository.dart';
import 'package:iec_covid_19_flutter/repositories/contacts_report_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/exam_repository.dart';
import 'package:iec_covid_19_flutter/repositories/exam_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/gender_repository.dart';
import 'package:iec_covid_19_flutter/repositories/gender_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/identification_type_repository.dart';
import 'package:iec_covid_19_flutter/repositories/identification_type_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/iec_report_api_repository.dart';
import 'package:iec_covid_19_flutter/repositories/iec_report_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/iec_repository.dart';
import 'package:iec_covid_19_flutter/repositories/iec_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/iecs_report_api_repository.dart';
import 'package:iec_covid_19_flutter/repositories/iecs_report_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/medication_repository.dart';
import 'package:iec_covid_19_flutter/repositories/medication_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/region_repository.dart';
import 'package:iec_covid_19_flutter/repositories/region_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/sample_type_repository.dart';
import 'package:iec_covid_19_flutter/repositories/sample_type_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/symptom_repository.dart';
import 'package:iec_covid_19_flutter/repositories/symptom_repository_interface.dart';
import 'package:iec_covid_19_flutter/repositories/zone_repository.dart';
import 'package:iec_covid_19_flutter/repositories/zone_repository_interface.dart';
import 'package:meedu/meedu.dart';

class RepositoriesServiceProvider {
  static register() {
    Get.i.put<IdentificationTypeRepositoryInterface>(
        IdentificationTypeRepository());
    Get.i.put<GenderRepositoryInterface>(GenderRepository());
    Get.i.put<IecRepositoryInterface>(IecRepository());
    Get.i.put<MedicationRepositoryInterface>(MedicationRepository());
    Get.i.put<SymptomRepositoryInterface>(SymptomRepository());
    Get.i.put<ComorbidityRepositoryInterface>(ComorbidityRepository());
    Get.i.put<ExamRepositoryInterface>(ExamRepository());
    Get.i.put<SampleTypeRepositoryInterface>(SampleTypeRepository());
    Get.i.put<ZoneRepositoryInterface>(ZoneRepository());
    Get.i.put<RegionRepositoryInterface>(RegionRepository());
    Get.i.put<CityRepositoryInterface>(CityRepository());
    Get.i.put<ContactsReportRepositoryInterface>(ContactsReportApiRepository());
    Get.i.put<IecReportRepositoryInterface>(IecReportApiRepository());
    Get.i.put<IecsReportRepositoryInterface>(IecsReportApiRepository());
  }
}
