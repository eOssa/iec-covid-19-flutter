import 'package:iec_covid_19_flutter/cache/cache_interface.dart';
import 'package:iec_covid_19_flutter/cache/sembast_adapter.dart';
import 'package:iec_covid_19_flutter/db/db_interface.dart';
import 'package:iec_covid_19_flutter/db/sembast_connector.dart';
import 'package:iec_covid_19_flutter/http/client_interface.dart';
import 'package:iec_covid_19_flutter/http/dio_client.dart';
import 'package:iec_covid_19_flutter/providers/models_service_provider.dart';
import 'package:iec_covid_19_flutter/providers/repositories_service_provider.dart';
import 'package:meedu/meedu.dart';

class AppServiceProvider {
  static void register() {
    ModelsServiceProvider.register();
    RepositoriesServiceProvider.register();
    Get.i.put<DatabaseConnector>(SembastConnector());
    Get.i.put<HttpClientInterface>(DioClient());
    Get.i.put<CacheInterface>(SembastAdapter());
  }
}
