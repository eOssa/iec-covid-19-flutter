import 'package:iec_covid_19_flutter/models/city_model.dart';
import 'package:iec_covid_19_flutter/models/comorbidity_model.dart';
import 'package:iec_covid_19_flutter/models/exam_model.dart';
import 'package:iec_covid_19_flutter/models/gender_model.dart';
import 'package:iec_covid_19_flutter/models/identification_type_model.dart';
import 'package:iec_covid_19_flutter/models/iec_model.dart';
import 'package:iec_covid_19_flutter/models/medication_model.dart';
import 'package:iec_covid_19_flutter/models/region_model.dart';
import 'package:iec_covid_19_flutter/models/sample_type_model.dart';
import 'package:iec_covid_19_flutter/models/symptom_model.dart';
import 'package:iec_covid_19_flutter/models/zone_model.dart';
import 'package:meedu/meedu.dart';

class ModelsServiceProvider {
  static register() {
    Get.i.put<CityModel>(CityModel());
    Get.i.put<ComorbidityModel>(ComorbidityModel());
    Get.i.put<ExamModel>(ExamModel());
    Get.i.put<GenderModel>(GenderModel());
    Get.i.put<IecModel>(IecModel());
    Get.i.put<IdentificationTypeModel>(IdentificationTypeModel());
    Get.i.put<MedicationModel>(MedicationModel());
    Get.i.put<RegionModel>(RegionModel());
    Get.i.put<SampleTypeModel>(SampleTypeModel());
    Get.i.put<SymptomModel>(SymptomModel());
    Get.i.put<ZoneModel>(ZoneModel());
  }
}
